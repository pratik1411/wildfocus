<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<section class="explore-eco-focus">
  <div class="explore">
		<h2 class="page-title"><?php _e( 'Not Found', 'twentyfourteen' ); ?></h2>
 		<div class="explore-text">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

				<?php get_search_form(); ?>
			</div>
    </div>
</section>
<?php
get_footer();
