<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php 
  $yes = get_field("available");
  if($yes):
    $qstdt=get_field("startdate"); 
    $qenddt=get_field("enddate");
  else:
    $qstdt=get_field("startdate2"); 
    $qenddt=get_field("enddate2");
  endif;

  $stdt=get_field("startdate"); 
  $enddt=get_field("enddate");
  $datetime1 = new DateTime($stdt);
  $datetime2 = new DateTime($enddt);
  $interval = date_diff($datetime1, $datetime2);
  
  $duration = $interval->format('%a days');
  
  $group = get_field("group");
  
  $rate = get_field("rate");
  
  $deposit = get_field("deposit");
  
  $title = get_the_title();
  global $post;
  $slug = get_post( $post )->post_name;
  
  $taxonomy = 'tour-category'; 
  $terms = wp_get_post_terms( $post->ID, $taxonomy );
  foreach ($terms as $cat) {
     $category=$cat->name; 
  }
  $_SESSION["tid"] = $post->ID;
  //$qstr = '?'.'title='.$title.'&duration='.$duration.'&group='.$group.'&rate='.$rate.'&category='.$category.'&deposit='.$deposit.'&sdate='.$qstdt.'&edate'.$qenddt;
  //$qstr = '?'.'tid='.$post->ID;
?>
<?php $title=$post->post_name; ?>
<div class="banner-wrap">
  <div class="banner banner-inner indexbanner">
        <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="fade"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
        data-cycle-pager=".banner-pager"
        data-cycle-caption="#adv-custom-caption"
        data-cycle-caption-template="{{cycleTitle}}"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=".$title."&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li data-cycle-title="<?php the_field('caption'); ?>">
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
        
        <div class="photo-tour-banner-text">
            <div class="photo-tour-banner-text-inner">
            <div class="trip-detail-slider-logo"></div>
        <div class="banner-pager"></div>
        <div id="adv-custom-caption" class="center"></div>
        <div class="c"></div>
            </div>
        </div>
        
    </div>
    
</div>
<section class="trip-details" id="tabs">
    <h2>Trip Details</h2>
    <div class="trip-details-left">
        <div class="trip-details-left-nav">
            <ul>
                <li><a href="#tabs-1"><span><img src="<?php echo get_template_directory_uri(); ?>/images/trip-details-overview-icon.png"></span>Overview</a></li>
                <li><a href="#tabs-2"><span><img src="<?php echo get_template_directory_uri(); ?>/images/trip-details-itinerary-icon.png"></span>Itinerary</a></li>
                <li><a href="#tabs-3"><span><img src="<?php echo get_template_directory_uri(); ?>/images/trip-details-accommodations-icon.png"></span>Accommodations</a></li>
                <li><a href="#tabs-4"><span><img src="<?php echo get_template_directory_uri(); ?>/images/trip-details-go-wild-add-ons-icon.png"></span>Go Wild Add Ons</a></li>
                <li><a href="#tabs-5"><span><img src="<?php echo get_template_directory_uri(); ?>/images/trip-details-dates-fees-icon.png"></span>Dates & Fees</a></li>
            </ul>
        </div>
    
      <div class="book-now-btn"><a href="<?php echo get_site_url(); ?>/make-a-reservation-onlne/"></a></div>
        <div class="photo-tour-detail-social">
            <a href="<?php echo get_option('wf_fbid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png"></a>
            <a href="<?php echo get_option('wf_tweet'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"></a>
            <a href="<?php echo get_option('wf_linked'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.png"></a>            
        </div>
    </div>
    
    <div class="trip-details-right">
    <div id="tabs-1">
        <h3><?php the_title(); ?></h3>
        <div class="trip-details-overview">
            <div class="trip-details-overview-left">
            <p><span>Dates:</span> <?php echo date("F d-", strtotime("$stdt")); ?>
            <?php $stm=date("F", strtotime("$stdt"));
                  $endm=date("F", strtotime("$enddt"));
            if($stm==$endm): ?>
            <?php $end=date("d, Y", strtotime("$enddt")); echo $end; ?>
            <?php else: 
              echo date("F d, Y", strtotime("$enddt")); 
              endif;?></p>
            <p><span>Duration:</span> <?php echo $duration; ?></p>
            <p><span>Group Size:</span><?php echo $group; ?></p>
            </div>
          <div class="trip-details-overview-right">
                <div class="trip-details-overview-img"><?php the_post_thumbnail(array(100,110)); ?></div>
            </div>
            <div class="c"></div>
        </div>
        <div class="trip-details-content-long">       
          <h4>Highlights</h4>
          <?php while(have_posts()):the_post(); ?>
            <?php ob_start();
             the_content('Read the full post',true);
             $postOutput = preg_replace('/<img[^>]+./','', ob_get_contents());
             ob_end_clean();
             echo $postOutput; ?>
          <?php endwhile; ?>    
        </div>
       <!-- <div class="trip-details-slider">
    <div class="trip-details-slide-nav">
      <div class="trip-details-slide-prev"></div>
      <div class="trip-details-slide-next"></div>
      <div class="c"></div>
    </div>
      <ul class="trip-details-slide"
        data-cycle-slides=">li"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="3"
        data-cycle-carousel-fluid="true"
        data-cycle-prev=".trip-details-slide-prev"
        data-cycle-next=".trip-details-slide-next"
        data-cycle-timeout=1000
        data-cycle-pause-on-hover="true">
        <?php
          $image_gallery = get_post_meta( $post->ID, '_easy_image_gallery', true );
          $attachments = array_filter( explode( ',', $image_gallery ) );
          if ( $attachments )
              foreach ( $attachments as $attachment_id ) {
        ?>
            <li>
              <?php $image_attributes = wp_get_attachment_image_src( $attachment_id,'thumb'  ); ?>
              <a href="<?php echo $image_attributes[0]; ?>" class="various" rel="wildgallery">
                <?php echo wp_get_attachment_image( $attachment_id,'tourimg' ); ?>  
              </a>
            </li>
         <?php
            }
        ?>
      </ul>    
  </div> -->
  <?php $ecofocusimg= get_field('ecofocus_image');?>
  <?php $ecofocusdesc= get_field('ecofocus_desc');?>
  <?php if((!(empty($ecofocusimg)))): ?>
    <?php if((!(empty($ecofocusdesc)))): ?>
<div class="photo-tours-details-eco-focus">
  <h4>Charity Supported</h4>
  <div class="photo-tours-details-eco-focus-left"><img src="<?php echo $ecofocusimg; ?>"></div>
  <div class="photo-tours-details-eco-focus-right">
      <?php echo $ecofocusdesc; ?>
    </div>
  <div class="c"></div>
</div>
<?php endif; ?>
<?php endif; ?>
    </div>
    <div id="tabs-2">
        <h3><?php the_title(); ?></h3>
        
        <div class="trip-details-content-long">       
          <?php the_field("iitinerary"); ?>
        </div>
         
    </div>
    
    <div id="tabs-3">
        <h3><?php the_title(); ?></h3>
        <div class="trip-details-content-long">       
          <?php the_field("accommodations"); ?>
        </div>
        <div class="trip-details-slider">
    <div class="trip-details-slide-nav">
      <div class="trip-details-slide-prev"></div>
      <div class="trip-details-slide-next"></div>
      <div class="c"></div>
    </div>
        <ul class="trip-details-slide"
        data-cycle-slides=">li"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="3"
        data-cycle-carousel-fluid="true"
        data-cycle-prev=".trip-details-slide-prev"
        data-cycle-next=".trip-details-slide-next"
        data-cycle-timeout=1000
        data-cycle-pause-on-hover="true">
        <?php $featuredImages=dfi_get_featured_images('[$post->Id]'); 
            $links = array();
            foreach($featuredImages as $images){
               $thumb = $images['tourimg'];
                $fullImage = $images['full'];
                $attachment_id=$images['attachment_id'];
              ?>
            <li>
            <a href="<?php echo $fullImage; ?>" class="various" rel="wildgallery1">
              <?php $image_thumb = wp_get_attachment_image_src( $attachment_id, 'tourimg');  ?>
              <img src="<?php echo $image_thumb[0]; ?>"/>
            </a>
            </li>
           <?php  } ?>
      </ul> 
      </div> 
    </div>
    
    <div id="tabs-4">
        <h3><?php the_title(); ?></h3>
        <div class="trip-details-content-long">       
         <?php the_field("go_wild_add_ons"); ?>
        </div>
    </div>

    <div id="tabs-5">
          <h3><?php the_title(); ?></h3>
          <div class="trip-details-right-left">
            <div class="trip-details-dates-fees-content">
              <ul>
                <li class="trip-details-dates-fees-content1">
                <h4>Dates:</h4>
                <div><?php echo date("F d-", strtotime("$stdt")); ?>
            <?php $stm=date("F", strtotime("$stdt"));
                  $endm=date("F", strtotime("$enddt"));
            if($stm==$endm): ?>
              <?php $end=date("d, Y", strtotime("$enddt")); echo $end; ?>
            <?php else: 
              echo date("F d, Y", strtotime("$enddt")); 
              endif;?>

              <?php 
              if($yes): else: echo "<br />(Not available)<br />";  

                $stdt2=get_field("startdate2"); 
                $enddt2=get_field("enddate2");

                echo date("F d-", strtotime("$stdt2"));
                $stm2=date("F", strtotime("$stdt2"));
                $endm2=date("F", strtotime("$enddt2"));

                  if($stm2==$endm2): 
                    $end2=date("d, Y", strtotime("$enddt2")); echo $end2; 
                  else: 
                    echo date("F d, Y", strtotime("$enddt2")); 
                  endif;

              endif; ?></div>
              </li>
                <li class="trip-details-dates-fees-content2">
                  <h4>Duration:</h4>
                  <div><?php echo $duration; ?></div>
              </li>
                <li class="trip-details-dates-fees-content3">
                  <h4>Group Size:</h4>
                  <div><?php echo $group; ?></div>
              </li>
                <li class="trip-details-dates-fees-content4"> <span>
                  <h4>Rates:</h4>
                  <div>$<?php echo $rate; ?></div>
                  </span> <span>
                    <h4>Deposit</h4>
                    <div>$<?php echo $deposit; ?></div>
                </span> </li>
            </ul>
          </div>
        </div>
          <div class="trip-details-right-right"><?php the_post_thumbnail(array(230,237)); ?></div>
          <div class="c"></div>
          <div class="trip-details-content-long">
            <h4>Inclusions</h4>
            <?php the_field("inclusions"); ?>
            <h4>Exclusions</h4>
            <?php the_field("exclusions"); ?>
          </div>
    </div>
  </div>
    <div class="c"></div>
</section>
<?php
get_footer();
