<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
    	<ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
		data-cycle-pager=".banner-pager"
        >
        <?php   $taxonomy = 'slideshow';
        $term_args=array(
          'hide_empty' => false,
          'orderby' => 'name',
          'order' => 'ASC'
        );
         $meta_slideshow_category = get_post_meta($post->ID, 'meta_slideshow_category', true);
        $tax_terms = get_terms($taxonomy,$term_args); ?>
        <?php foreach ($tax_terms as $tax_term) { ?>
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=blog&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
          <?php } ?>
        </ul>
    </div>   
</div>
<section class="trip-details">
	<h2>Photo Blog</h2>
	<div class="c"></div>
    	<div class="blog">
        	<div class="blog-left">
            	<div class="blog-list">
               	  <ul>
					<?php
						if ( have_posts() ) :
							while ( have_posts() ) : the_post();
								get_template_part( 'content', get_post_format() );
							endwhile;
						else :
							get_template_part( 'content', 'none' );
						endif;
					?>
				  </ul>
                </div>
            </div>
            <?php echo get_template_part("sidebar-blog"); ?>
            <div class="c"></div>
          <div class="pagination-wrap">
            	<div class="pagination">
                	<?php if (function_exists('wp_pagenavi')) {
				      	wp_pagenavi();
				     	wp_reset_postdata();
				    } ?>
              </div>
            </div>
            
        </div>
</section>
<?php
get_footer();