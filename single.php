<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
    	<ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
		data-cycle-pager=".banner-pager"
        >
        <?php   $taxonomy = 'slideshow';
        $term_args=array(
          'hide_empty' => false,
          'orderby' => 'name',
          'order' => 'ASC'
        );
         $meta_slideshow_category = get_post_meta($post->ID, 'meta_slideshow_category', true);
        $tax_terms = get_terms($taxonomy,$term_args); ?>
        <?php foreach ($tax_terms as $tax_term) { ?>
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=blog-detail&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
          <?php } ?>
        </ul>
    </div>
</div>
<section class="trip-details">
	<h2>Photo Blog</h2>
	<div class="c"></div>
    	<div class="blog">
        	<div class="blog-left">
        		<div class="blog-detail">
               	  <div class="blog-post-text">
                    <h3><a href="#"><?php the_title(); ?></a></h3>
            	<div class="blog-post-date-author">
                	<span><?php the_time('F d, Y'); ?></span>Posted by: <?php the_author(); ?></div>
			<?php
				while ( have_posts() ) : the_post();
					the_content();?>
			</div>
			<?php 
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
				endwhile;
			?>
                </div>
            </div>
            <?php echo get_template_part("sidebar-blog"); ?>
            <div class="c"></div> 
        </div>
</section>
<?php
get_footer();
