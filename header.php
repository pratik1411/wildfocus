
<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jpreloader.css" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/jpreloader.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.carousel.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.hoverIntent.minified.js"></script>

<script type="text/javascript">
	$(document).ready(function() {	
	<?php 
		$cookie = 'banner_' . md5($_SERVER['REQUEST_URI']);
		if(!isset($_COOKIE[$cookie])) { 
	?>
	$('.indexbanner').jpreLoader();
	<?php 
		setcookie($cookie,1); 
		} 
	?>



$( ".photo-tour-banner-text" ).delay( 5000 ).fadeIn( 400 );
	});
</script>

<style>
.photo-tour-banner-text{
	display: none;
}

</style>
<script>
$(function() {
$( "#inline" ).tooltip();
});
</script>


<!--<script type="text/javascript">
$(function() {
        $(".index-banner ul li > img").each(function(i, elem) {
          var img = $(elem);
		  $(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });                  
        });
    });
</script>
-->

<script type="text/javascript">
$(function() {
      	//$('.index-banner ul li').css('background','none');
        $(".index-banner ul li > img").each(function(i, elem) {
          var img = $(elem);
    		$(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });                  
        });
    });
</script>

<script type="text/javascript">
$(function() {
	    //$('.index-banner ul li').css('background','none');
        $(".index-banner ul li span > img").each(function(i, elem) {
          var img = $(elem);
		  $(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });                  
        });
    });
</script>

<script>
     $(document).ready(function () {
        $('.index-banner ul li').height($(window).height()-100);});
  $(window).resize(function() {
  $('.index-banner ul li').css('height', (window.innerHeight-100)+'px');
  $(".header-right nav ul li ul li:nth-child(4)").addClass("sub-nav4");
});
</script>

<script type="text/javascript">
$(document).ready(function () {
    $(".multi-select select").change(function () {
        var str = "";
        str = $(this).find(":selected").text();
        $(this).next(".out").text(str);
    }).trigger('change');
})
</script>

<script>
$(document).ready(function () {
      $('.wild-packages-slider').cycle({
		  
      })
	  
      $('.eco-focus-slider').cycle({
      })
	  
      $('.banner-slider').cycle({
      })
	  
	  $('.trip-details-slide').cycle({
      })

      $('.index-blog').cycle({
      })
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(".accodian li h5").click(function () {		
		var current_li = $(this).parent();
		$(".accodian li div").each(function(i,el) {			
			if($(el).parent().is(current_li)) {				
				$(el).prev().toggleClass("plus");
				$(el).slideToggle();				
			} else{
				$(el).prev().removeClass("plus");
				$(el).slideUp();
			}
		});
    });
	$('.accodian li > div').hide();
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){	
		$('nav > ul > li').hoverIntent(function() {
			$(this).find('> ul').fadeIn();
			$(this).addClass('active');
		},function() {
			$(this).find('> ul').fadeOut();
			$(this).removeClass('active');
		});	
		});	
</script>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$(function() {
$( "#datepicker" ).datepicker({
	dateFormat: "dd-mm-yy",
	onClose: function( selectedDate ) {
        $( "#datepicker1" ).datepicker( "option", "minDate", selectedDate );
      }
});
});
</script>

 <script>
$(function() {
$( "#datepicker1" ).datepicker({
	dateFormat: "dd-mm-yy",
	onClose: function( selectedDate ) {
        $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
      }
});
});
</script>

<script type="text/javascript">
$(function() {
	$( "#tabs" ).tabs();
	$( ".reasons-to-travel ul li:nth-child(4)" ).addClass("reasons-4");
	if ($("body").hasClass("page-template-page-templatesfaq-php")) { 
		$("body").addClass("faq-image");   
	}
	if ($("body").hasClass("page-template-page-templatesterms-php")) { 
		$("body").addClass("privacy");   
	}
	if ($("body").hasClass("page-template-page-templatesprivacypolicy-php")) { 
		$("body").addClass("privacy");   
	}
});
</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox2/jquery.fancybox.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fancybox2/jquery.fancybox.css" type="text/css" media="screen" />

<script type="text/javascript">

$(document).ready(function() {
 $(".fancybox").fancybox({
  openEffect : 'none',
  closeEffect : 'none'
 });
 
 $("a[rel=gallery]").fancybox({
            'transitionIn'  : 'none',
            'transitionOut'  : 'none',
            'titlePosition'  : 'over',
            'titleFormat'  : function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });
});

$(document).ready(function() {
 $(".various").fancybox({
  maxWidth : 800,
  maxHeight : 600,
  fitToView : false,
  width  : '70%',
  height  : '70%',
  autoSize : false,
  closeClick : false,
  openEffect : 'none',
  closeEffect : 'none'
 });

 $(".various1").fancybox({
  maxWidth :940,
  maxHeight :647,
  fitToView : false,
  //width  : '70%',
  //height  : '70%',
  width :920,
  height :647,
  autoSize : false,
  closeClick : false,
  openEffect : 'none',
  closeEffect : 'none',
  padding:'0'
 });

});

</script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jqtransform.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.jqtransform.js"></script>

  <!-- <script language="javascript">
		$(function(){
			$('.form1').jqTransform({imgPath:'jqtransformplugin/img/'});
		});
	</script> -->
    
<script type="text/javascript">
$(function() {
    //find all form with class jqtransform and apply the plugin
   	$(".jqtransform").jqTransform();
});
</script>
<script type="text/javascript">

jQuery(document).ready(function() {
     jQuery('form#searchform div.index-search-form div.selectjq1 div.rowElem div.jqTransformSelectWrapper ul li a').click(function () {

        //action = 'my_submit_log_action';
		var pdata = {
		     action: "my_submit_log_action",
		     postdata: $('#region').val()
		  }
       jQuery.post( 
        	"<?php echo admin_url('admin-ajax.php'); ?>",
            pdata,
            function ( response ) {
	            $('#interest').empty().append(response);	            
	            var $ul = $('#interest').prev();
	            $ul.empty();
	            var $select = $('#interest');
	            var $wrapper = $select.parent().parent();
	            console.log($ul);
	            $('option', $('#interest')).each(function(i){
					var oLi = $('<li><a href="#" index="'+ i +'">'+ $(this).html() +'</a></li>');
					$ul.append(oLi);
				});
				$ul.find('li a').first().addClass('selected');
				$('#interest').parent().parent().find('span').first().text($ul.find('li a').first().text());
				$ul.find('a').click(function(){
					$('a.selected', $wrapper).removeClass('selected');
					$(this).addClass('selected');	
					/* Fire the onchange event */
					if ($select[0].selectedIndex != $(this).attr('index') && $select[0].onchange) { $select[0].selectedIndex = $(this).attr('index'); $select[0].onchange(); $select[0].change(); }
					$select[0].selectedIndex = $(this).attr('index');
					$('span:eq(0)', $wrapper).html($(this).html());
					$ul.hide();
					return false;
				});
				var iSelectHeight = ($('li',$ul).length)*30;//+1 else bug ff
				$ul.css({height:iSelectHeight,'overflow':'hidden'});				
            }	 
        ); 
    }); 
});
</script>

<script type="text/javascript">
$(document).ready(function() {

    $('#guest').change(function () {
    	var num     = $(this).val();
    	var newNum  = new Number(num + 1);
    	$(".clonedclasss").remove();

    for (var j = 1; j <= num-1; j++) {
            var newElem = $('#input1').clone().attr('id', 'input' + newNum).attr("class", "clonedclasss");
            newElem.children(':first').attr('id', 'name' + newNum).attr('name', 'name' + newNum);
            newElem.find('input,select').each(function (k, v) {
            	      //var id = this.id;
            	      //var theId = (j + 1);
            	      //$(v).attr('name',id + '_' + theId);
            	      //$(v).attr('id',id + '_' + theId);
            	      $(v).val('');
               });
            $('#input1').after(newElem);
        }
      });
    });
    </script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	$.validator.addMethod("NumbersOnly", function(value, element) {
        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
    }, "Phone must contain only numbers, + and -.");
	
    var validator = $("#cform").validate({
        errorElement: "p",
		rules: {
			'first_name[]': {
                required: true,
                minlength: 2
            }
		}
	}); 

    //search box region validation
	jQuery('form#searchform div.index-search-form input.search-submit').click(function () {   
	    var select=$('form#searchform div.index-search-form div.jqtransform div.rowElem div.jqTransformSelectWrapper div span').text();
	    if(select=='By Region'){
	    	$('form#searchform div.index-search-form div.jqtransform div.rowElem p.selerr').text("Please select region");
	    	return false;	//prevent redirection
	    }
	    else{
	    	$('form#searchform div.index-search-form div.jqtransform div.rowElem p.selerr').text("");
	    }
	});

	$("input.fname").each(function(){
       $(this).rules("add", {
           required:true
       });                   
    });
});
 </script>
<?php echo get_option('wf_ga_code'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<img src="<?php echo get_template_directory_uri(); ?>/images/loader-bg.jpg" style="width:0px; height:0px;">
	<header>
		<div class="logo"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a></div>
	    <div class="header-right">
	    	<nav>
	        	<?php //wp_nav_menu(array("menu"=>"Menu1","container"=>"")); ?>
	        	<ul>
            	<li class="nav1"><a href="<?php echo get_site_url(); ?>/reasons-to-travel">Why Travel <br>with Wild?</a></li>
            	<li class="nav2"><a href="<?php echo get_site_url(); ?>/wildteam">Wild <br>Team</a></li>
            	<li class="nav3"><a href="<?php echo get_site_url(); ?>/faqs">FAQs</a></li>
            	<li class="nav4"><a href="<?php echo get_site_url(); ?>/contactus">Contact <br>Us</a></li>
            	<li class="nav5"><a href="<?php echo get_site_url(); ?>/photo-tours">Photo <br>Tours</a>
                	<ul>
                		<?php   $taxonomy = 'tour-category';
					      $term_args=array(
					        'hide_empty' => false,
					        'orderby' => 'name',
					        'order' => 'ASC'
					      );
					      $tax_terms = get_terms($taxonomy,$term_args); 
					    ?>
  						<?php foreach ($tax_terms as $tax_term) { ?>
	                    	<li><h3>
	                    			<img src="<?php echo get_field('menu_img',$taxonomy . '_' . $tax_term->term_id); ?>">
	                    			<?php echo $tax_term->name; ?>
	                    		</h3>
	                        	<ul>
	                        		<?php query_posts("post_type=tour&taxonomy=tour-category&posts_per_page=-1&term=$tax_term->slug"); ?>
	        						<?php while(have_posts()):the_post(); ?>
	                            		<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	                            	<?php endwhile; wp_reset_query(); ?>
	                            </ul>
	                        </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>  
	        </nav>
	    </div>
	    <div class="c"></div>
	</header>
