<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<li>
	<div class="blog-list-left">
		<div class="blog-list-left-pin"></div><?php the_post_thumbnail(array(207,213)); ?>
	</div>
	<div class="blog-list-right">
		<div class="blog-post-text">
			<div class="blog-post-date-author">
				<span><?php the_time("F d, Y"); ?></span>Posted by: <?php the_author(); ?>
			</div>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<div class="blog-post-text1">
				<?php echo get_excerpt("400"); ?>
			</div>
			<div class="blog-post-ream-more"><a href="<?php the_permalink(); ?>">Read more</a></div>
		</div>
	</div>
	<div class="c"></div>
</li>