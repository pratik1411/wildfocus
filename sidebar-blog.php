<div class="blog-right">
	<div class="blog-subscribe">
    	<div class="blog-subscribe-title"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-subscribe.png"></div>
		<?php dynamic_sidebar("sidebar-1"); ?>
    </div>
    <div class="blog-popular">
        <div class="blog-popular-title"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-popular-post.png"></div>
        <div class="blog-popular-list">
        	<ul>
            	<?php query_posts("post_type=post&posts_per_page=3&orderby=comment_count"); ?>
                <?php while(have_posts()):the_post(); ?>
            	<li>
                	<div class="blog-popular-list-img"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(61,61)); ?></a></div> 
                	<div class="blog-popular-list-text">
                    	<h3><a href="<?php the_permalink(); ?>"><?php if (strlen($post->post_title) > 22) {
                    echo substr(the_title($before = '', $after = '', FALSE), 0, 40) . ''; } else {
                    the_title();
                    } ?></a></h3>
                        <span>Posted by: <?php the_author(); ?></span>
                        <span><?php the_time("M d, Y"); ?></span>
                    </div>
                    <div class="c"></div>
                </li>
                <?php endwhile; wp_reset_query(); ?>
            </ul>
        </div>
    </div>
    <div class="latest-tweet">
    	<div class="latest-tweet-title"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-latest-tweet.png"></div>
        <div class="latest-tweet-bg">
        	<?php dynamic_sidebar("sidebar-2"); ?>
        </div>
    </div>
</div>