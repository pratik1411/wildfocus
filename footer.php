<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<section class="footer-wrap">
	<div class="footer-bottom">
	<footer>
    	<ul>
        	<li class="quick-contact">
            	<h3>Quick Links</h3>
                <?php $menuParameters = array(
					   'menu'=>'fmenu',    
					   'before'     => '<p>',
	                  'after'      => '</p>',         	
					  'container'       => false,
					  'echo'            => false,
					  'items_wrap'      => '%3$s',
					  'depth'           => 0,
                    );
					echo strip_tags(wp_nav_menu( $menuParameters ),'<a>' ); ?>
            </li>
        	<li>
            	<h3>Get Connected</h3>
               <div class="get-connected">
               		<a href="<?php echo get_option('wf_fbid'); ?>" class="facebook">Facebook</a>
               		<a href="<?php echo get_option('wf_tweet'); ?>" class="twitter">Twitter</a>
               		<a href="<?php echo get_option('wf_linked'); ?>" class="linkedin">Linkedin</a>
               		<a href="<?php echo get_option('wf_youtube'); ?>" class="youtube">YouTube</a>
               </div>
            </li>
            
       	  <li>
            	<h3>Contact Us</h3>
                <div class="footer-contact">
                	<div class="footer-contact-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png"></div>
                  <div class="footer-contact-address">
                    <?php dynamic_sidebar("sidebar-3"); ?>
                  </div>
                    <div class="c"></div>
                	<div>
                        <div class="footer-contact-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/skype-icon.png"></div>
                        <div class="footer-contact-address">WildFocusTours</div>
                        <div class="c"></div>
                    </div><br>
                	<div>
                        <div class="footer-contact-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/email-icon.png"></div>
                        <div class="footer-contact-address"><a href="mailto:info@wildfocus.com">info@wildfocus.com</a></div>
                        <div class="c"></div>
                    </div>
                </div>
          </li>
        </ul>
        <div class="c"></div>
        <div class="copyright">
        © 2013  Wild Focus Photography Adventures. All Rights Reserved. 
        	<span>Design & Developed by <a href="http://etrafficwebdesign.com.au/" target="_blank">eTraffic Web Design</a></span>
        </div>
    </footer>
    </div>
</section>
<?php wp_footer(); ?>
</body>
</html>