<?php
/**
 * Template Name: Sustainable travel Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
        <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
        data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=sustainable-travel&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
  <h2>Why Travel With Wild?</h2>
    <div class="trip-details-left">
      <div class="trip-details-left-nav">
          <ul>
              <li><a href="<?php echo get_site_url(); ?>/reasons-to-travel/">10 Reasons to Travel With Us</a></li>
              <li><a href="<?php echo get_site_url(); ?>/our-travel-styles/">Our Travel Styles</a></li>
              <li><a href="<?php echo get_site_url(); ?>/sustainable-travel/" class="active">Sustainable Travel</a></li>
              <li><a href="<?php echo get_site_url(); ?>/giving-back-to-the-wild/">Giving Back to the Wild</a></li>
              <li><a href="<?php echo get_site_url(); ?>/why-travelwith-wild/">With us</a></li>
              <li><a href="<?php echo get_site_url(); ?>/amazing-destinations/" >Amazing Destinations</a></li>
            </ul>
        </div>
    </div>
    
   <div class="trip-details-right">
    <div class="amazing-destinations-text">
    <img src="<?php the_field('titleimg'); ?>">
    <img src="<?php the_field('friendlyimg'); ?>" class="sustainable-travel-img2">
    <div class="c"></div>
    <br>
    <?php the_post_thumbnail("full"); ?>
<div class="c"></div>
    <?php while(have_posts()):the_post(); ?>
      <?php echo get_the_content(); ?>
    <?php endwhile; ?>
    <div class="c"></div>   
      <div class="sustainable-travel-img">
        <ul>
          <?php
        $image_gallery = get_post_meta( $post->ID, '_easy_image_gallery', true );
        $attachments = array_filter( explode( ',', $image_gallery ) );
            foreach ( $attachments as $attachment_id ) { ?><li>
            <?php //$image_attributes = wp_get_attachment_image_src( $attachment_id,'full'  ); ?>
            <?php echo wp_get_attachment_image( $attachment_id, 'full' ); ?>  
            </li>
      <?php } ?>
          </ul>
          <div class="c"></div>
      </div>
      <div class="greengeeks">
        <?php the_field("greengeeks"); ?>
      </div>
      </div>
<div class="recycling-energy-saving">
  <?php the_field("recycling"); ?>
  </div>
  </div>
    <div class="c"></div>
</section>
<?php
get_footer();
