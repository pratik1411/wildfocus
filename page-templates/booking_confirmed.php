<?php
/**
 * Template Name: Booking Confirmed Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); 
if(!empty($_SESSION["id"]))
{
  global $wpdb;
  $table_name1 = $wpdb->prefix . "payment_detail";
  $table_name2 = $wpdb->prefix . "posts";

  $update=$wpdb->query(
    "UPDATE $table_name1
    SET pd_status = 'Confirmed',
    pd_date = CURDATE()
    WHERE pd_group_id  = ".$_SESSION['id']
  );

  $data = "SELECT * 
    FROM  $table_name1 t1,$table_name2 t2
    WHERE t1.pd_group_id = ".$_SESSION['id']." AND t1.pd_tour_id=t2.ID";

  $pageposts = $wpdb->get_results($data, OBJECT);
  //var_dump($pageposts); exit;
  foreach($pageposts as $post):
    $pd_tour_id = $post->pd_tour_id;
    $post_title = $post->post_title;
    $date = $post->pd_date;
    //$first_name = $post->pd_first_name;
    //$last_name = $post->pd_last_name;
    $mobile = $post->pd_mobile;
    $email = $post->pd_email;
    $country = $post->pd_country;
    $address1 = $post->pd_address1;
    $address2 = $post->pd_address2;
    $city = $post->pd_city;
    $state = $post->pd_state;
    $zip = $post->pd_zip;
    $bookid = $post->pd_id;
    $status = $post->pd_status;
  endforeach;

  $stdt=get_post_meta("$pd_tour_id", 'startdate',true);
  $enddt=get_post_meta("$pd_tour_id", 'enddate',true);
  $amt = $_SESSION['AMT'];
  $card = $_SESSION['CREDITCARDTYPE'];
  $tid = $_SESSION['transaction_id'];
  $groupid = $_SESSION['id'];
  session_destroy();
  
}
else{
   echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location='".get_site_url()."';
        </SCRIPT>");
  exit;
}

?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
          <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=booking-confirmed&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
          <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
  <h2><?php the_title(); ?></h2>
  <div class="make-a-reservation-onlne">
      <div class="make-a-reservation-onlne-text">
          <b>Thank you for booking your photography holiday with Wild Focus!</b>
      </div>
<div class="c"></div>
<div class="c"></div>
<div class="book-new-box" id="test">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/booking-confirmed-logo.jpg">
  <span>Iterinary Receipt</span></h4>
  <div class="booking-confirmed-content">
      <h5>Booking Confirmed</h5>
        <div class="booking-details-content">
          <div class="booking-details-content-left">
              <ul>
                  <li><span>Booking Date</span>: <?= $date  ?></li>
                  <li><span>Address</span>: <?php echo $address1; echo $address2; ?> </li>
                  <li><span>Contact</span>: <?= $mobile ?></li>
                  <li><span>Email</span>: <a href="mailto:info@wildfocus.com"><?= $email ?></a></li>
                </ul>
            </div>
          <div class="booking-details-content-right">
              Booking Reference Number:
                <span><?= $groupid ?></span>
            <p>Status:
                  <span><?= $status ?></span>
                </p>
            </div>
            <div class="c"></div>
        </div>
    </div>
    
<div class="booking-confirmed-content">
        <div class="guest-detail-content">
      <h5>Guest Details</h5>
          <ol>
            <?php foreach($pageposts as $post): ?>
              <li><?php echo $post->pd_first_name." "; echo $post->pd_last_name; ?></li>
            <?php endforeach; ?>
            </ol>
        </div>
    </div>
    
<div class="booking-confirmed-content">
        <div class="guest-detail-content">
      <h5>Tour Details</h5>
          <div class="booking-confirmed-tour-detail">
                  <div class="booking-confirmed-tour-detail-name">Tour Name: <?= $post_title ?></div>
              <div class="booking-confirmed-tour-detail-left">
                    <span>Departure :</span>
                    <?= $stdt ?>
                </div>
                
              <div class="booking-confirmed-tour-detail-right">
                <span>Arrival :</span>
                <?= $enddt ?>
                </div>
                <div class="c"></div>
            </div>
            
        </div>
    </div>
    
<div class="booking-confirmed-content booking-confirmed-last">
        <div class="guest-detail-content">
      <h5>Payment Details</h5>
          <div class="booking-confirmed-payment-details">
              <div class="booking-confirmed-payment-details-left">
                  <div class="booking-confirmed-payment-cost">
                      Cost : <?= $amt ?> USD
                        <!-- <span>Tax (10%): 120.0 USD</span> -->
                    </div>
                    <div class="booking-confirmed-payment-cost-total">TOTAL : <?= $amt ?> USD</div>
                    <div class="c"></div>
                </div>
                
              <div class="booking-confirmed-payment-details-right">
                  <ul>
                      <li><span>Payment Type</span>: <?= $card ?></li>
                      <li><span>Date</span>: <?= $date  ?></li>
                      <li><span>Transaction ID</span>: <?= $tid ?></li>
                    </ul>
                </div>
                <div class="c"></div>
            </div>
        </div>
    </div>
    
</div>

<div class="print-btn">
<input type="button" value="" onclick="window.print()" />
</div>
<div class="c"></div>
  </div>
</section>
<?php
get_footer();