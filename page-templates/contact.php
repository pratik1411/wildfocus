<?php
/**
 * Template Name: Contact Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=contact&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>
<div class="contact">
  <div class="contact-left">
      <div class="contact-phone">
          <div class="contact-phone-title"><img src="<?php echo get_template_directory_uri(); ?>/images/contact-phone.png"></div>
            <div class="contact-phone-text">
             <?php while(have_posts()):the_post(); ?> 
                <?php the_content(); ?>
             <?php endwhile; ?>
            </div>
        </div>
      <div class="contact-phone">
          <div class="contact-phone-title"><img src="<?php echo get_template_directory_uri(); ?>/images/contact-email.png"></div>
            <div class="contact-phone-text">
              <?php the_field("email"); ?>
          </div>
        </div>
      <div class="contact-phone">
          <div class="contact-phone-title"><img src="<?php echo get_template_directory_uri(); ?>/images/contact-solial-links.png"></div>
            <div class="contact-social-icon">
              <div class="contact-social-icon-fb"><a href="<?php echo get_option('wf_fbid'); ?>"></a></div>
              <div class="contact-social-icon-twitter"><a href="<?php echo get_option('wf_tweet'); ?>"></a></div>
              <div class="contact-social-icon-youtube"><a href="<?php echo get_option('wf_youtube'); ?>"></a></div>
              <div class="contact-social-icon-linkedin"><a href="<?php echo get_option('wf_linked'); ?>"></a></div>
            </div>
        </div>
    </div>
  <div class="contact-right">
      <div class="contact-form">
          <div class="contact-form-title"><img src="<?php echo get_template_directory_uri(); ?>/images/contact-title.png"></div>
            <div class="get-in-touch">
              <?php echo do_shortcode('[contact-form-7 id="39" title="sidebar"]'); ?>
          </div>
          <div class="contact-inc"><img src="<?php echo get_template_directory_uri(); ?>/images/contact-inc.png"></div>
        </div>
    </div>
    <div class="c"></div>
</div>
<?php
get_footer();
