<?php
/**
 * Template Name: FAQ Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=faq&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>
<section class="trip-details">
  <h2>Frequently Asked Questions</h2>
  <div class="c"></div>
    <div class="faq">
      <div class="faq-left">
                
          <div class="faq-list">
            <h3><?php echo $tax_term->description; ?></h3>
            <ul class="accodian">
            <?php query_posts("post_type=faq&term=$tax_term->name&posts_per_page=-1"); ?>
              <?php while(have_posts()):the_post(); ?>
                <li>
                  <h5 class="minus" id="plus1"><span><?php the_title(); ?></span></h5>
                        <div><?php the_content(); ?></div>
                </li>
              <?php endwhile; wp_reset_query(); ?>
            </ul>
          </div>
              </div>
      <div class="faq-right">
          <div class="get-in-touch">
            <div class="get-in-touch-title"><img src="<?php echo get_template_directory_uri(); ?>/images/get-in-touch-title.png"> </div>
              <?php echo do_shortcode('[contact-form-7 id="39" title="sidebar"]'); ?>
          </div>
        </div>
        <div class="c"></div>
    </div>
</section>
<?php
get_footer();
