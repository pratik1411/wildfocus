<?php
/**
 * Template Name: Amazing Destination Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=amazing-destination&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
  <h2>Why Travel With Wild?</h2>
    <div class="trip-details-left">
      <div class="trip-details-left-nav">
          <ul>
              <li><a href="<?php echo get_site_url(); ?>/reasons-to-travel/">10 Reasons to Travel With Us</a></li>
              <li><a href="<?php echo get_site_url(); ?>/our-travel-styles/">Our Travel Styles</a></li>
              <li><a href="<?php echo get_site_url(); ?>/sustainable-travel/">Sustainable Travel</a></li>
              <li><a href="<?php echo get_site_url(); ?>/giving-back-to-the-wild/">Giving Back to the Wild</a></li>
              <li><a href="<?php echo get_site_url(); ?>/why-travelwith-wild/">With us</a></li>
              <li><a href="<?php echo get_site_url(); ?>/amazing-destinations/" class="active">Amazing Destinations</a></li>
            </ul>
        </div>
    </div>
   <div class="trip-details-right">
    <div class="amazing-destinations-text">
      <img src="<?php the_field('sideimg'); ?>" class="amazing-destinations-img2">
      <img src="<?php the_field('titleimg'); ?>"  class="amazing-destinations-img1">
      <?php while(have_posts()):the_post(); ?>
        <?php echo get_the_content(); ?>
      <?php endwhile; ?>
      <div class="c"></div>        
    </div>
  <!-- <div class="trip-details-content-long">       
      <h4>The Wild places we travel to...</h4>
      <a href="<?php the_field('larger_image'); ?>" class="fancybox"><?php the_post_thumbnail("full"); ?></a>
      </div> -->
      <div class="trip-details-content-long">       
      <h4>The Wild places we travel to...</h4>
      <a class="various1" href="#inline"><?php the_post_thumbnail("full"); ?></a>
      <div id="inline" style="display:none; position:relative;"><img src="<?php echo get_template_directory_uri(); ?>/images/map.png">
      <a href="http://54.200.79.82/projects/wildfocus/tour/discovering-zambias-south-luangwa-photo-safari/" class="pin" id="pin1" target="_blank" title="Discovering Zambia’s South Luangwa Photo Safari"></a>
      <a href="http://54.200.79.82/projects/wildfocus/tour/zambias-south-luangwa-the-green-season-photo-safari/" target="_blank" class="pin" id="pin2" title="Zambia’s South Luangwa – The Green Season Photo Safari"></a>
      <a href="http://54.200.79.82/projects/wildfocus/tour/grizzlies-of-the-khutzeymateen-photo-adventure-tour/" target="_blank" class="pin" id="pin3" title="Grizzlies of the Khutzeymateen Photo Adventure Tour"></a>
      <a href="http://54.200.79.82/projects/wildfocus/tour/ultimate-grizzly-spirit-bear-photo-adventure-tour/" target="_blank" class="pin" id="pin4" title="Ultimate Grizzly & Spirit Bear Photo Adventure Tour"></a>
      <a href="http://54.200.79.82/projects/wildfocus/tour/antarctica-south-georgia-the-falklands-photo-expedition/" target="_blank" class="pin" id="pin5" title="Antarctica, South Georgia & The Falklands Photo Expedition"></a>
      <a href="http://54.200.79.82/projects/wildfocus/tour/the-antarctic-explorer-the-antarctic-peninsula-south-shetland-islands-photo-expedition/" target="_blank" class="pin" id="pin6" title="The Antarctic Peninsula & South Shetland Islands Photo Expedition"></a>
      </div>
      </div>
      <div class="mobile-map">
      <h4>The Wild places we travel to...</h4>
      <p><?php the_post_thumbnail("full"); ?></p>
      </div>
    </div>
    <div class="c"></div>
</section>
<?php
get_footer();
