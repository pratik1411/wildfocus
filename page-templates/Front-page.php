<?php
/**
 * Template Name: Front Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="banner-wrap">
  <div class="banner index-banner indexbanner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="fade"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
        data-cycle-timeout=3000
		data-cycle-loader="true"
        >
          <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=home&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
          <li><?php the_post_thumbnail("bannerimg"); ?>
              <span><img src="<?php the_field('mobile_image'); ?>"/></span>
              <div class="banner-text">
                <div class="banner-text-content">
                <?php the_field("caption"); ?>
                <div class="banner-next"></div>
                <div class="banner-prev"></div>
                </div>
                </div>
            </li>
          <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>

</div>

<section class="index-search-video-wrap">
  <div class="index-search-video">
      <div class="index-search">
        <div class="index-search-title">
            <img src="<?php echo get_template_directory_uri(); ?>/images/index-search-title-bg.png">
            <div>Where in the Wild<br>Do you want to go?</div>
        </div>
            <form role="search" method="get" name="testform" id="searchform" action="<?php echo home_url( '/' ); ?>">
	            <input type="hidden" name="post_type" value="tour" />
	            <div class="index-search-form">
				    <div class="jqtransform selectjq1">
					    <div class="rowElem">
					      <select name="region" class="region" id="region">
				                <option selected value="">By Region</option>
				                <?php   $taxonomy = 'tour-category';
							      $term_args=array(
							        'hide_empty' => false,
							        'orderby' => 'name',
							        'order' => 'ASC'
							      );
							      $tax_terms = get_terms($taxonomy,$term_args); 
							    ?>
								<?php foreach ($tax_terms as $tax_term) { ?>
				            		<option value="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></option>
				                <?php } ?>
					      </select>
                <p class="selerr"></p>
					    </div>
					</div>
	            </div>
	            <div class="c"></div>
			     <div class="index-search-form">
			        <div class="jqtransform selectjq2">
					    <div class="rowElem">
					    <select name="interest" id="interest" >
					      <option></option>
					      </select>
					    </div>
					</div>
					<div class="select-date">
						<div class="select-date-start">
							<label>Start Date:</label>
							<input type="text" id="datepicker" name="st" placeholder="DD/MM/YYYY">
						</div>
						<div class="select-date-end">
					  		<label>End Date:</label>
						    <input type="text" id="datepicker1" name="end" placeholder="DD/MM/YYYY">
						</div>
					</div>
			        <input type="submit" value="Submit" class="search-submit">
	            </div>
            </form>
      </div>

      <div class="index-video">
          <h2> &#9679;<span>Discover the Wild Advantage</span> &#9679;</h2>
            <div class="index-video-img">
              <a href="<?php the_field('video'); ?>" class="various fancybox.iframe">
                <img src="<?php the_field('videoimg'); ?>">
              </a>
            </div>
        </div>
        <div class="c"></div>
    </div>
</section>

<section class="wild-packages-wrap">
  <div class="wild-packages">
      <ul class="wild-packages-slider"
        data-cycle-slides=">li"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="4"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".package-slider-nav-prev"
        data-cycle-next=".package-slider-nav-next"
        >
         <?php query_posts("post_type=tour&posts_per_page=10"); ?>
          <?php while(have_posts()):the_post(); ?>
            <?php if(has_post_thumbnail()): ?>
          <li><?php the_post_thumbnail(array(338,352)); ?>
            <div>
              <?php 
                $stdt=get_field("startdate"); 
                $enddt=get_field("enddate"); ?>
              <h3><a href="<?php the_permalink(); ?>"><?php if (strlen($post->post_title) > 25) {
          echo substr(the_title($before = '', $after = '', FALSE), 0, 25) . ''; } else {
          the_title();
          } ?></a></h3>
          <?php 
            $stdt=get_field("startdate"); 
            $enddt=get_field("enddate");
            $datetime1 = new DateTime($stdt);
            $datetime2 = new DateTime($enddt);
            $interval = date_diff($datetime1, $datetime2);
           ?>
          <span><?php echo date("F d-", strtotime("$stdt")); ?>
            <?php $stm=date("F", strtotime("$stdt"));
                  $endm=date("F", strtotime("$enddt"));
            if($stm==$endm): ?>
            <?php $end=date("d, Y", strtotime("$enddt")); echo $end; ?>
            <?php else: 
              echo date("F d, Y", strtotime("$enddt")); 
              endif;?>
            </span>
            <p><?php the_excerpt(); ?></p>
            </div>
          </li>
        <?php endif; ?>
        <?php endwhile; wp_reset_query(); ?>
  </ul>
  <div class="package-slider-nav-bg">
      <div class="package-slider-nav-prev"></div>
        <button data-cycle-cmd="pause" data-cycle-context=".wild-packages-slider" class="pause"></button>
        <button data-cycle-cmd="resume" data-cycle-context=".wild-packages-slider" class="resumed"></button>
      <div class="package-slider-nav-next"></div>
    </div>
    </div>
</section>

<section class="explore-eco-focus">
  <div class="explore">
      <h2>Explore<span>&#9679;</span>Discover<span>&#9679;</span>Capture</h2>
        <div class="explore-text">
           <?php while(have_posts()):the_post(); ?>
              <?php the_content(); ?>
           <?php endwhile; ?>
        </div>
    </div>
    
  <div class="eco-focus">
      <div class="eco-focus-title">Eco Focus</div>
        <div class="eco-focus-content">
          <ul class="eco-focus-slider"
        data-cycle-slides=">li"
        data-cycle-fx="fade"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".eco-focus-slider-prev"
        data-cycle-next=".eco-focus-slider-next"
        >
        <?php query_posts("post_type=focuse&posts_per_page=-1"); ?>
        <?php while(have_posts()):the_post(); ?>
        <li>
            <h4><?php the_title(); ?></h4>
            <div class="eco-focus-slider-img"><?php the_post_thumbnail(array(238,190)); ?></div>
            <div class="eco-focus-slider-text"><?php the_content(); ?></div>
        </li>
        <?php endwhile; wp_reset_query(); ?>
  </ul>
<div class="eco-focus-slider-next"></div>
<div class="eco-focus-slider-prev"></div>
        </div>
  </div>
    <div class="c"></div>
</section>

<section class="testimonials">
    <div class="testimonials-title">What's the Word on Wild?</div>
  <?php query_posts("post_type=testimonial&posts_per_page=1"); ?>
  <?php while(have_posts()):the_post(); ?>
    <div class="testimonials-text">
        <?php echo get_the_content(); ?>
          <span><?php the_title(); ?></span>
    </div>
  <?php endwhile; wp_reset_query(); ?>
</section>

<section class="photo-blog">
  <div class="photo-blog-title">Photo Blog</div>
    <div class="photo-blog-post">
    <ul class="index-blog"
        data-cycle-slides=">li"
        data-cycle-fx="fade"
        data-cycle-carousel-fluid=true
        data-cycle-pager=".index-blog-pager"
    >
    <?php query_posts("post_type=post&posts_per_page=3"); ?>
    <?php while(have_posts()):the_post(); ?>
      <li>
        <div class="blog-post-img"><?php the_post_thumbnail('homeblogimg'); ?></div>
          <div class="blog-post-text">
            <div class="blog-post-date-author">
                <span><?php the_time("F d, Y");?></span>Posted by: <?php the_author(); ?>
                  <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                  <div class="blog-post-text1">
                    <?php echo get_excerpt("500"); ?>
                  </div>
                  <div class="blog-post-ream-more"><a href="<?php the_permalink(); ?>">Read more</a></div>
              </div>
        </div>
        <div class="c"></div>
      </li>
    <?php endwhile; wp_reset_query(); ?>
    </ul>
    <div class="index-blog-pager"></div>
</section>

<?php
get_footer();
