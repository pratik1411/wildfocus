<?php
/**
 * Template Name: Payment Details Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=payment-details&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
  <h2><?php the_title(); ?></h2>
  <div class="make-a-reservation-onlne">
      <div class="make-a-reservation-onlne-text">
          <p><b>Deposit Due:</b> A $500 deposit per guest is required to complete your resevation. Only the deposit amount will be charged to your credit card at theis time.</p>

<p><b>Balance Amount:</b> An invoice will be sent to your for the outstanding balance. This amount is due on the date specified in the invoice.</p>
<p><b>Taxes & Single Supplements:</b> These amounts will only be applied to your costs if applicable.</p>
<p>
All Canadian tours carry an additional GST tax of 5% (50% ot this tax ca be telaimed -- detail will be provided).</p>
<p>Tour prices are based upon shared accommodation. If you are a single traveller we will endevour upon request to find another suitable quest for you to share accommodations with.
<p>If you do not wish to share a room, we will automatically apply the single supplement amount on your invoice. Single supplement costs are provided on each tour page if they are available. If you would like a private room please tick the box below.</p>
<form name="payment" id="payment" method="post" action="">
<div class="applied-to-my-invoice"><label><input name="" type="checkbox" value="">I would prefer not to share a room, and understand that a single supplement charge will be applied to my invoice.</label></div>
        </div>
<div class="c"></div>
<div class="c"></div>
<div class="book-new-box">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/book-new-box-title-logo.png">Payment Information</h4>
    <div class="book-new-box-form">
    
    <div>
      <div class="book-new-box-form-inner-left">
        <div class="payment-detail-price">
          <span>Amount to be Charged</span>
            500 usd Deposit x 2 Guest
            <span>= 1000 usd</span>
</div>
      <p>
          <label>Credit Card Type <span>*</span></label>
            <select name="">
              <option>Select Card Type</option>
              <option>2</option>
              <option>3</option>
            </select>
        </p>
        
      <p>
          <label>Card Number <span>*</span></label>
      <input name="" type="text">
        </p>
        
      <div class="card-expire-date">
          <p>
            <label>Expiration Date <span>*</span></label>
            <select name="">
              <option>Month</option>
              <option>2</option>
              <option>3</option>
            </select>
            
            <select name="">
              <option>Year</option>
              <option>2</option>
              <option>3</option>
            </select>
            <div class="c"></div>
            </p>
            
        </div>
        
      <p>
          <label>Card Verification # ? <span>*</span></label>
      <input name="" type="text">
        </p>
        
        
        </div>
      <div class="book-new-box-form-inner-right">
        
      <p>
          <label>Name <span>*</span> <b>(as it appers on credit card)</b></label>
      <input name="" type="text">
          <div class="book-new-box-form-check-box">
            <label><input name="" type="checkbox">Billing address same as guest 1</label>
            </div>
        </p>
        
      <p>
          <label>Country</label>
            <select name="">
              <option>United States</option>
              <option>2</option>
              <option>3</option>
            </select>
        </p>
        
      <p>
          <label>Address <b>(line 2 )</b></label>
      <input name="" type="text">
        </p>
        
      <p>
          <label>City</label>
      <input name="" type="text">
        </p>
        
      <p>
          <label>State</label>
            <select name="">
              <option>Select</option>
              <option>2</option>
              <option>3</option>
            </select>
        </p>
        
      <p>
          <label>Zip</label>
      <input name="" type="text">
        </p>
        
      <p>
          <label>Day Time Phone <span>*</span> <b>(as printed in passport)</b></label>
      <input name="" type="text">
        </p>
        </div>
        <div class="c"></div>
    </div>
        
    </div>
</div>

<div class="c"></div>

<div class="book-new-box">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/book-new-box-title-logo.png">Terms And Conditions</h4>
<div class="book-new-box-tc">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Submission of your Request is merely  an offer to The Sample Network to use commercially reasonable efforts to provide the services and deliver the deliverables described in the Request for the price proposed therein. The Request is not binding on The Sample Network unless or until The Sample Network delivers written confirmation of its acceptance of the Request to the Client. The Sample Network reserves the right, in its sole discretion, to accept or reject any Request. Acceptance of a Request  only obligats The Sample Network to use commercially reasonable efforts to provide the services described in the Request. In the event The Sample Network accepts a Request, Client shall have thirty (60) days to make its survey available to The Sample Network as required by the Request.
</div>
<div class="book-new-box-tc-accepted">
<div class="book-new-box-form-check-box">
  <label><span><a href="#">Print this page</a></span><input name="" type="checkbox" value="">I have read and accepted the <a href="#">Terms and Conditions</a></label>
</div>
</div>
</div>
  </div>
  
<div class="make-a-reservation-onlne-text">
  *Trip cancellation insurance
<p>We strongly recommend purchasing trip insurance that covers cancellation and interruptions. Many trip insurance policies also include medical expenses, emergency evacuation, and protection for baggage and personal possessions.</p>
<p>After submitting your booking form and when your payment has been verified, you will receive a receipt number on the next page. A copy of your booking will be sent to you by email to the email address you provided.</p>
</div>
  
<div class="c"></div>
<div align="right"><img src="<?php echo get_template_directory_uri(); ?>/images/google-captcha.jpg">
  <input type="submit" value="" class="payment-submit"></div>
</form>
  <div class="c"></div>
</section>
<?php
get_footer();
