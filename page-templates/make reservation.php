<?php
/**
 * Template Name: Make Reservation Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); 
if(!$_SESSION["tid"])
{
	 echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location='".get_site_url()."';
        </SCRIPT>");
  	exit;
}
if($_POST['submit'])
{

  // if(empty($_POST["first_name"]))
  //   $first_name = "Plase enter First name.";
  // else if(!is_string($_POST["first_name"]))
  //   $first_name = "Enter correct First name.";  
  // else
  //   $first_name = "";

  // if(empty($_POST["last_name"]))
  //   $last_name = "Plase enter Last name.";
  // else if(!is_string($_POST["last_name"]))
  //   $last_name = "Enter correct Last name.";  
  // else
  //   $last_name = "";

  // if(empty($_POST["dphone"]))
  //   $dphone = "Plase enter day time phone.";
  // else if(!is_numeric($_POST["dphone"]))
  //   $dphone = "Enter correct day time phone.";  
  // else
  //   $dphone = "";

  // if(empty($_POST["night_phone_a"]))
  //   $night_phone_a = "Plase enter night time phone.";
  // else if(!is_numeric($_POST["night_phone_a"]))
  //   $night_phone_a = "Enter correct night time phone.";  
  // else
  //   $night_phone_a = "";

  // if(empty($_POST["mobile1"]))
  //  {
  //   $mobile1 = "Plase enter mobile phone.";
  //  }
  // else if(!is_numeric($_POST["mobile1"]))
  // {
  //   $mobile1 = "Mobile phone must be numeric."; 
  // }
  // else
  // {
  //   $mobile1 = "";
  // }

  // if(empty($_POST["buyer_email"]))
  //   $buyer_email = "Plase enter email.";
  // // else if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$_POST["buyer_email"]))
  // //   $buyer_email = "Enter correct email.";  
  // else
  //   $buyer_email = "";

  // if(empty($_POST["billing_country"]))
  //   $billing_country = "Select country.";
  // else
  //   $billing_country = "";

  // if(empty($_POST["billing_address1"]))
  //   $billing_address1 = "Plase enter address.";
  // else
  //   $billing_address1 = "";

  // if(empty($_POST["billing_city"]))
  //   $billing_city = "Plase enter city name.";
  // else if(!is_string($_POST["billing_city"]))
  //   $billing_city = "Enter correct city name.";  
  // else
  //   $billing_city = "";

  // if(empty($_POST["billing_state"]))
  //   $billing_state = "Select state.";
  // else
  //   $billing_state = "";

  // if(empty($_POST["zip"]))
  //   $zip = "Plase enter zip.";
  // else if(!is_numeric($_POST["zip"]))
  //   $zip = "Enter correct zip.";  
  // else
  //   $zip = "";

  if(empty($first_name) )
  {
    global $wpdb;
    extract($_POST);
    $n = count($_POST["first_name"]);
    $groupid = mt_rand();
    
    $name=  $_POST["first_name"];
 	$last_name=  $_POST["last_name"];
	$day_phone=  $_POST["dphone"];
	$night_phone=  $_POST["night_phone_a"];
	$mobile=  $_POST["mobile1"];
	$email=  $_POST["buyer_email"];
	$country=  $_POST["billing_country"];
	$address1=  $_POST["billing_address1"];
	$address2=  $_POST["billing_address2"];
	$city=  $_POST["billing_city"];
	$state=  $_POST["billing_state"];
	$zip=  $_POST["zip"];

    $table_name = $wpdb->prefix . "payment_detail";
    for ($i = 0; $i < $n; $i++) {
        $query = 'INSERT INTO '.$table_name.' (pd_tour_id,pd_group_id,pd_guest,pd_first_name,pd_last_name,pd_day_phone,pd_night_phone,pd_mobile,pd_email,pd_country,pd_address1,pd_address2,pd_city,pd_state,pd_zip,pd_status) VALUES 
        (\'' . $_SESSION["tid"] . '\',\'' . $groupid . '\',\'' . $n. '\',\'' . $name[$i] . '\', \'' . $last_name[$i] . '\', \'' . $day_phone[$i] . '\', \'' . $night_phone[$i] . '\', \'' . $mobile[$i] . '\', \'' . $email[$i] . '\', \'' . $country[$i] . '\', \'' . $address1[$i]. '\', \'' . $address2[$i]. '\', \'' . $city[$i]. '\', \'' . $state[$i]. '\', \'' . $zip[$i]. '\', \'pending\')';
    	$wpdb->query($query);
    }
    $_SESSION["id"] = $groupid;

    //$sql_start = "INSERT INTO $table_name (pd_first_name,pd_last_name,pd_day_phone,pd_night_phone,pd_mobile,pd_country,pd_state,pd_zip) VALUES "; 
    
   //  foreach ($section as $row=>$name) {
   //    $pd_first_name=  $name[0][0];
   //    $pd_last_name=  $name[0][1];
   //    $pd_day_phone=  $name[0][2];
   //    $pd_night_phone=  $name[0][3];
   //    $pd_mobile=  $name[0][4];
   //    $pd_email=  $name[0][5];
   //    $pd_country=  $name[0][6];
   //    $pd_address1=  $name[0][7];
   //    $pd_address2=  $name[0][8];
   //    $pd_city=  $name[0][9];
   //    $pd_state=  $name[0][10];
   //    $pd_zip=  $name[0][11];

   // $sql_array = "('$pd_first_name','$pd_last_name','$pd_day_phone','$pd_night_phone','$pd_mobile','$pd_country','$pd_state','$pd_zip')"; 
   //  $wpdb->query($sql_start.$sql_array);

      
    //}
    // $query = $wpdb->insert( $table_name, array( 
    //     'pd_first_name' => $pd_first_name,
    //     'pd_last_name'=>$pd_last_name,
    //     'pd_day_phone'=>$pd_day_phone,
    //     'pd_night_phone'=>$pd_night_phone,
    //     'pd_mobile'=>$pd_mobile,
    //     'pd_email'=>$pd_email,
    //     'pd_country'=>$pd_country,
    //     'pd_address1'=>$pd_address1,
    //     'pd_address2'=>$pd_address2,
    //     'pd_city'=>$pd_city,
    //     'pd_state'=>$pd_state,
    //     'pd_zip'=>$pd_zip,
    //     'pd_status'=>'pending'
    //     )
    // );
//    $sql_array = "('$pd_first_name','$pd_last_name','$pd_day_phone','$pd_night_phone','$pd_mobile','$pd_country','$pd_state','$pd_zip')"; 
    //$sql_array = '('.$pd_first_name.','.$pd_last_name.','.$pd_day_phone.','.$pd_night_phone.','.$pd_mobile.','.$pd_email.','.$pd_country.','.$pd_address1.','.$pd_address2.','.$pd_city.','.$pd_state.','.$pd_zip.');'; 
   // echo ($sql_start.$sql_array);
    //$wpdb->query($sql_start.$sql_array);
    //echo $query;
  //  exit;
    

    //$_SESSION["id"] = mysql_insert_id();
    echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location='payment-details';
        </SCRIPT>");
    exit();
  }
}
?>
<div class="banner-wrap">
  <div class="banner banner-inner">
    <ul class="banner-slider"
      data-cycle-slides=">li"
      data-cycle-fx="scrollHorz"
      data-cycle-carousel-fluid=true
      data-cycle-prev=".banner-prev"
      data-cycle-next=".banner-next"
      data-cycle-pager=".banner-pager"
    >
      <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=make-reservation&order=asc"); ?>
      <?php while ( have_posts() ) : the_post(); ?>
        <li>
          <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
          <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
        </li>
      <?php endwhile; wp_reset_query(); ?>
    </ul>
  </div>
</div>

<section class="trip-details">
  <h2><?php the_title(); ?></h2>
  <div class="make-a-reservation-onlne">
    <div class="make-a-reservation-onlne-text"><b>C</b>ongratulations! You've made an excellent choice and are just a few steps away from securing  your spot on an amazing photography adventure with Wild Focus.
      <p>All tours require a $500 USD deposit per guest to reserve a space on the tour. Please fill in the Booking Form below and follow the easy steps to pay your deposit online by credit card. Alternatively, you can <a href="<?php echo get_site_url(); ?>/contactus">Contact Us</a> by phone or email, and we can arrange a differnet method of payment. We accept payments by all major credit cards online or over the phone, or payment can be made by bank wire transfer. Please note that outstanding invoice payments will only be accepted by wire transfer.</p>
    </div>
    <div class="c"></div>



  <!-- <input type="hidden" name="subtotal" value="<?php echo $_GET['rate']; ?>"> -->

  <div class="book-new-box">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/book-new-box-title-logo.png">Trip Information</h4>
    <div class="book-new-box-form">
      	<?php $post_id = $_SESSION["tid"];
			$queried_post = get_post($post_id); 
		?>
      <p>
        <label>Destination <span>*</span></label>
        <select name="destination" id="destination">
          <option><?php echo $queried_post->post_title; ?></option>
        </select>
        <?php if(isset($cnumber)) { ?><div class="error-message"> <?php echo $cnumber ?></div> <?php } ?>
      </p>
      <p>
        <label>Date <span>*</span></label>
        <?php $yes = get_post_meta("$post_id", 'available',true);
        	if($yes):
			    $stdt=get_post_meta("$post_id", 'startdate',true);
			    $enddt=get_post_meta("$post_id", 'enddate',true); 
			else:
			    $stdt=get_post_meta("$post_id", 'startdate2',true);
			    $enddt=get_post_meta("$post_id", 'enddate2',true); 
			endif;
 		?>
        <?php $end = date("F d-", strtotime("$stdt")); ?>
        <?php $stm=date("F", strtotime("$stdt"));
          $endm=date("F", strtotime("$enddt"));
          if($stm==$endm): ?>
            <?php $end .=date("d, Y", strtotime("$enddt"));  ?>
          <?php else: 
            $end .= date("F d, Y", strtotime("$enddt")); 
          endif;?>
        <select name="date" id="date">
          <option><?php echo $end; ?></option>
        </select>
      </p>
    </div>
</div>
<div class="c"></div>
<div class="book-new-box">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/book-new-box-title-logo.png">Guest Information</h4>
    <div class="book-new-box-form">
      <p>
        <label>Select No. of Guest:  <span>*</span></label>
        <?php $g = get_post_meta("$post_id", 'group',true); ?>
        <select name="guest" id="guest">
        <?php for($i=1;$i<=$g;$i++){ ?>
          <option value="<?echo $i; ?>"><?echo $i; ?></option>
        <?php } ?>
        </select>
      </p>
<form method="post" action="" name="cform" id="cform">
      <div id="input1" style="margin-bottom:4px;" class="clonedInput">  <!-- cloned start -->

    <div class="book-new-box-form-inner">
      <div class="book-new-box-form-inner-left">
      <!-- <p>
        <label>Guest #1</label>
        <select name="">
          <option>Title</option>
          <option>2</option>
          <option>3</option>
        </select>
      </p> -->
      <p>
        <label>First Name <span>*</span> <b>(as printed in passport)</b></label>
        <input name="first_name[]" id="first_name[]" type="text" class="required">
        <?php if(isset($first_name)) { ?><div class="error-message"> <?php echo $first_name ?></div> <?php } ?>
      </p>
      <p>
        <label>Last Name <span>*</span> <b>(as printed in passport)</b></label>
        <input name="last_name[]" id="last_name[]" type="text" class="required fname">
        <?php if(isset($last_name)) { ?><div class="error-message"> <?php echo $last_name ?></div> <?php } ?>
      </p>
      <p>
        <label>Day Time Phone <span>*</span></label>
        <input name="dphone[]" id="dphone[]" type="text" class="NumbersOnly">
        <?php if(isset($dphone)) { ?><div class="error-message"> <?php echo $dphone ?></div> <?php } ?>
      </p>
      <p>
        <label>Night Time Phone <span>*</span></label>
        <input name="night_phone_a[]" id="night_phone_a[]" type="text" class="NumbersOnly">
        <?php if(isset($night_phone_a)) { ?><div class="error-message"> <?php echo $night_phone_a ?></div> <?php } ?>
      </p>
      <p>
        <label>Mobile Phone <span>*</span></label>
        <input name="mobile1[]" id="mobile1[]" type="text"  class="NumbersOnly">
        <?php if(isset($mobile1)) { ?><div class="error-message"> <?php echo $mobile1 ?></div> <?php } ?>
      </p>
      <p>
        <label>Email <span>*</span></label>
        <input name="buyer_email[]" id="buyer_email[]" type="text" class="required">
        <?php if(isset($buyer_email)) { ?><div class="error-message"> <?php echo $buyer_email ?></div> <?php } ?>
      </p>
    </div>
      <div class="book-new-box-form-inner-right">
      <p>
        <label>Country</label>
        <select name="billing_country[]" id="billing_country[]">
          <option>Select Country</option>
          <option value="AU">Australia</option>
          <option value="BE">Belgium</option>
          <option value="BG">Bulgaria</option>
          <option value="HR">Croatia</option>
          <option value="CY">Cyprus</option>
          <option value="CZ">Czech Republic</option>
          <option value="DK">Denmark</option>
          <option value="EE">Estonia</option>
          <option value="FI">Finland</option>
          <option value="FR">France</option>
          <option value="DE">Germany</option>
          <option value="GR">Greece</option>
          <option value="HU">Hungary</option>
          <option value="IE">Ireland</option>
          <option value="IT">Italy</option>
          <option value="LV">Latvia</option>
          <option value="LT">Lithuania</option>
          <option value="LU">Luxembourg</option>
          <option value="MT">Malta</option>
          <option value="NL">Netherlands</option>
          <option value="PL">Poland</option>
          <option value="PT">Portugal</option>
          <option value="RO">Romania</option>
          <option value="SK">Slovakia</option>
          <option value="SI">Slovenia</option>
          <option value="ES">Spain</option>
          <option value="SE">Sweden</option>
          <option value="GB">United Kingdom</option>
        </select>
        <?php if(isset($billing_country)) { ?><div class="error-message"> <?php echo $billing_country ?></div> <?php } ?>
      </p>
      <p>
        <label>Address <span>*</span></label>
        <input name="billing_address1[]" id="billing_address1[]" type="text" required="required">
        <?php if(isset($billing_address1billing_address1)) { ?><div class="error-message"> <?php echo $billing_address1 ?></div> <?php } ?>
      </p>
      <p>
        <label>Address <b>(line 2 )</b></label>
        <input name="billing_address2[]" id="billing_address2[]" type="text">
      </p>
      <p>
        <label>City</label>
        <input name="billing_city[]" id="billing_city[]" type="text" required="required">
        <?php if(isset($billing_city)) { ?><div class="error-message"> <?php echo $billing_city ?></div> <?php } ?>
      </p>
      <p>
        <label>State</label>
        <select name="billing_state[]" id="billing_state[]">
          <option>Select state</option>
          <option value="BE">Belgium</option>
          <option value="DE">Germany</option>
          <option value="FR">France</option>
          <option value="LU">Luxembourg</option>
          <option value="NL">Netherlands</option>
          <option value="CH">Switzerland</option>
          <option value="GB">United Kingdom</option>
          <option value="SE">Sweden</option>
          <option value="IT">Italy</option>
          <option value="AT">Austria</option>
          <option value="LI">Liechtenstein</option>
          <option value="GR">Greece</option>
          <option value="ES">Spain</option>
          <option value="DK">Denmark</option>
          <option value="MC">Monaco</option>
          <option value="PT">Portugal</option>
          <option value="IE">Ireland</option>
          <option value="FI">Finland</option>
          <option value="CY">Cyprus</option>
          <option value="TR">Turkey</option>
          <option value="BG">Bulgaria</option>
          <option value="CZ">Czech Republic</option>
          <option value="EE">Estonia</option>
          <option value="NO">Norway</option>
          <option value="RS">Serbia</option>
          <option value="cal">California</option>
          <option value="te">Texas</option>
          <option value="te">Washington</option>
        </select>
        <?php if(isset($billing_state)) { ?><div class="error-message"> <?php echo $billing_state ?></div> <?php } ?>
      </p>
      <p>
        <label>Postal Code</label>
        <input name="zip[]" id="zip[]" type="text" required="required">
        <?php if(isset($zip)) { ?><div class="error-message"> <?php echo $zip ?></div> <?php } ?>
      </p>
      <p><label>&nbsp;</label>
        <!-- <div class="book-new-box-form-check-box">
          <label><input name="" type="checkbox">Use this address for all Guests</label>
        </div> -->
      </p>
    </div>
    <div class="c"></div>
  </div>

  
</div>  <!-- cloned end -->



  </div>
</div>
</div>
<div class="c"></div>
<input type="submit" name="submit" class="process-to-payment-detail-btn">
</form>
  <div class="c"></div>
</section>
<?php
get_footer();