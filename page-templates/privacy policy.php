<?php
/**
 * Template Name: Privay Policy Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=privacy-policy&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>
<section class="trip-details">
  <h2>Terms &amp; Conditions</h2>
    <div class="trip-details-left">
      <div class="trip-details-left-nav">
          <ul>
              <li><a href="<?php echo get_site_url(); ?>/terms-conditions/"><span><img src="<?php echo get_template_directory_uri(); ?>/images/terms-conditions-icon.png"></span>Terms & Conditions </a></li>
              <li><a href="<?php echo get_site_url(); ?>/privacy-policy/" class="active"><span><img src="<?php echo get_template_directory_uri(); ?>/images/privacy-policy-icon.png"></span>Privacy Policy</a></li>
            </ul>
        </div>
    </div>
    <div class="trip-details-right">
      <?php while(have_posts()):the_post(); ?> 
        <?php the_content(); ?>
      <?php endwhile; ?>
    </div>
    <div class="c"></div>
</section>
<?php
get_footer();