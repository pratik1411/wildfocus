<?php
/**
 * Template Name: Payment Details Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();
if(!empty($_SESSION["id"]))
{
  global $wpdb;
  $table_name = $wpdb->prefix . "payment_detail";
  $data = "SELECT * 
    FROM  $table_name
    WHERE pd_group_id = ".$_SESSION['id']." LIMIT 1";

  $pageposts = $wpdb->get_results($data, OBJECT);
  
  //var_dump($pageposts);
  foreach($pageposts as $post):
    $tour_id = $post->pd_tour_id;
    $date = $post->pd_date;
    $guest = $post->pd_guest;
    $first_name = $post->pd_first_name;
    $last_name = $post->pd_last_name;
    $day_phone = $post->pd_day_phone;
    $night_phone = $post->pd_night_phone;
    $mobile = $post->pd_mobile;
    $email = $post->pd_email;
    $country = $post->pd_country;
    $address1 = $post->pd_address1;
    $address2 = $post->pd_address2;
    $city = $post->pd_city;
    $state = $post->pd_state;
    $zip = $post->pd_zip;
    $status = $post->pd_status;
  endforeach;
}
else{
  echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location='".get_site_url()."';
        </SCRIPT>");
  exit;
}

$rate = 500;
$amt = $rate * $guest;

if($_POST['submit'])
{
  if(($_POST["terms"]!=1))
    $terms = "Agree to terms.";
  else
    $terms = "";

  if(empty($_POST["card"]))
    $card_err = "Select card type.";
  else
    $card_err = "";

  if( (empty($_POST["expmonth"]) && empty($_POST["expyear"]) ))
    $date= "Select expiration date.";
  else
    $date = "";
  
  if(!is_numeric($_POST["cnumber"]))
    $cnumber = "Card number must be numeric.";
  else if(strlen($_POST["cnumber"]) < 12)
    $cnumber = "Enter correct card number.";  
  else
    $cnumber = "";

  if(!is_numeric($_POST["verify"]))
    $verify = "Card Verification number must be numeric.";
  else if(!(strlen($_POST["verify"])) > 4)
    $verify = "Enter correct card Verification number.";  
  else
    $verify = "";

  if(empty($card_err) && empty($date) && empty($cnumber) && empty($verify))
  {

  require_once('config.php');
  require_once('paypal.class.php');

  $PayPalConfig = array(
    'Sandbox' => $sandbox,
    'APIUsername' => $api_username,
    'APIPassword' => $api_password,
    'APISignature' => $api_signature
  );

  $PayPal = new PayPal($PayPalConfig);

  $DPFields = array(
    'paymentaction' => 'Sale',            // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
    'ipaddress' => $_SERVER['REMOTE_ADDR'],               // Required.  IP address of the payer's browser.
    'returnfmfdetails' => '1'           // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
  );
          
  $CCDetails = array(
    'creditcardtype' => $_POST["card"],           // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
    'acct' => $_POST["cnumber"],                 // Required.  Credit card number.  No spaces or punctuation.  
    'expdate' => $_POST["expmonth"].$_POST["expyear"],              // Required.  Credit card expiration date.  Format is MMYYYY
    'cvv2' => $_POST["verify"],                // Requirements determined by your PayPal account settings.  Security digits for credit card.
    'startdate' => '',              // Month and year that Maestro or Solo card was issued.  MMYYYY
    'issuenumber' => ''             // Issue number of Maestro or Solo card.  Two numeric digits max.
  );
          
  $PayerInfo = array(
    'email' => $_POST["buyer_email"],                 // Email address of payer.
    'payerid' => '',              // Unique PayPal customer ID for payer.
    'payerstatus' => '',            // Status of payer.  Values are verified or unverified
    'business' => ''              // Payer's business name.
  );
          
  $PayerName = array(
    'salutation' => '',             // Payer's salutation.  20 char max.
    'firstname' => $_POST["first_name"],              // Payer's first name.  25 char max.
    'middlename' => '',             // Payer's middle name.  25 char max.
    'lastname' => $_POST["last_name"],              // Payer's last name.  25 char max.
    'suffix' => ''                // Payer's suffix.  12 char max.
  );
          
  $BillingAddress = array(
    'street' => $_POST["billing_address1"],             // Required.  First street address.
    'street2' => $_POST["billing_address2"],            // Second street address.
    'city' => $_POST["billing_city"],              // Required.  Name of City.
    'state' => $_POST["billing_state"],              // Required. Name of State or Province.
    'countrycode' => $_POST["billing_country"],          // Required.  Country code.
    'zip' => $_POST["zip"],               // Required.  Postal code of payer.
    'phonenum' => $_POST["mobile"]            // Phone Number of payer.  20 char max.
  );
            
  $ShippingAddress = array(
    'shiptoname' => '',           // Required if shipping is included.  Person's name associated with this address.  32 char max.
    'shiptostreet' => '',           // Required if shipping is included.  First street address.  100 char max.
    'shiptostreet2' => '',          // Second street address.  100 char max.
    'shiptocity' => '',           // Required if shipping is included.  Name of city.  40 char max.
    'shiptostate' => '',          // Required if shipping is included.  Name of state or province.  40 char max.
    'shiptozip' => '',            // Required if shipping is included.  Postal code of shipping address.  20 char max.
    'shiptocountrycode' => '',        // Required if shipping is included.  Country code of shipping address.  2 char max.
    'shiptophonenum' => ''          // Phone number for shipping address.  20 char max.
  );
            
  $PaymentDetails = array(
    'amt' => $amt,              // Required.  Total amount of order, including shipping, handling, and tax.  
    'currencycode' => 'USD',          // Required.  Three-letter currency code.  Default is USD.
    'itemamt' => '',            // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
    'shippingamt' => '',          // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
    'handlingamt' => '',          // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
    'taxamt' => '',             // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
    'desc' => '',              // Description of the order the customer is purchasing.  127 char max.
    'custom' => '',             // Free-form field for your own use.  256 char max.
    'invnum' => '',             // Your own invoice or tracking number
    'buttonsource' => '',           // An ID code for use by 3rd party apps to identify transactions.
    'notifyurl' => ''           // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
  );

  $OrderItems = array();    
  $Item  = array(
    'l_name' => $_POST["tourid"],            // Item Name.  127 char max.
    'l_desc' => '',             // Item description.  127 char max.
    'l_amt' => $rate,              // Cost of individual item.
    'l_number' => '',            // Item Number.  127 char max.
    'l_qty' => $guest,               // Item quantity.  Must be any positive integer.  
    'l_taxamt' => '',             // Item's sales tax amount.
    'l_ebayitemnumber' => '',         // eBay auction number of item.
    'l_ebayitemauctiontxnid' => '',     // eBay transaction ID of purchased item.
    'l_ebayitemorderid' => ''         // eBay order ID for the item.
  );
  array_push($OrderItems, $Item);

  $PayPalRequestData = array(
   'DPFields' => $DPFields, 
   'CCDetails' => $CCDetails, 
   'PayerInfo' => $PayerInfo,
   'PayerName' => $PayerName, 
   'BillingAddress' => $BillingAddress, 
   'PaymentDetails' => $PaymentDetails, 
   'OrderItems' => $OrderItems
  );

  $PayPalResult = $PayPal -> DoDirectPayment($PayPalRequestData);

  $_SESSION['transaction_id'] = isset($PayPalResult['TRANSACTIONID']) ? $PayPalResult['TRANSACTIONID'] : '';
  $_SESSION['ACK'] = ($PayPalResult['ACK']=='Success') ? $PayPalResult['ACK'] : '';
  $_SESSION['AMT'] = isset($PayPalResult['AMT']) ? $PayPalResult['AMT'] : '';
  $_SESSION['CREDITCARDTYPE'] = isset($PayPalResult['REQUESTDATA']['CREDITCARDTYPE']) ? $PayPalResult['REQUESTDATA']['CREDITCARDTYPE'] : '';
  
  if($_SESSION['ACK']=='Success')
  {
    echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location='booking-confirmed';
        </SCRIPT>");
    exit();
  }
 
  //print_r($PayPalResult); 

  }
}
?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=payment-details&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
  <h2><?php the_title(); ?></h2>
  <div class="make-a-reservation-onlne">
      <div class="make-a-reservation-onlne-text">
          <p><b>Deposit Due:</b> A $500 deposit per guest is required to complete your resevation. Only the deposit amount will be charged to your credit card at theis time.</p>

<p><b>Balance Amount:</b> An invoice will be sent to your for the outstanding balance. This amount is due on the date specified in the invoice.</p>
<p><b>Taxes & Single Supplements:</b> These amounts will only be applied to your costs if applicable.</p>
<p>
All Canadian tours carry an additional GST tax of 5% (50% ot this tax ca be telaimed -- detail will be provided).</p>
<p>Tour prices are based upon shared accommodation. If you are a single traveller we will endevour upon request to find another suitable quest for you to share accommodations with.
<p>If you do not wish to share a room, we will automatically apply the single supplement amount on your invoice. Single supplement costs are provided on each tour page if they are available. If you would like a private room please tick the box below.</p>
<form name="payment" id="payment" method="post">

<!-- <input name="date" id="date" type="hidden" value="<?= $_GET['date'] ?>">  -->
 <input name="tourid" id="tourid" type="hidden" value="<?= $tour_id ?>">
<input name="first_name" id="first_name" type="hidden" value="<?= $first_name ?>">
<input name="last_name" id="last_name" type="hidden" value="<?= $last_name ?>">
<input name="dphone" id="dphone" type="hidden" value="<?= $day_phone ?>">
<input name="night_phone_a" id="night_phone_a" type="hidden" value="<?= $night_phone ?>">
<input name="mobile" id="mobile" type="hidden" value="<?= $mobile ?>">
<input name="buyer_email" id="buyer_email" type="hidden" value="<?= $email ?>">
<input name="billing_country" id="billing_country" type="hidden" value="<?= $country?>">
<input name="billing_address1" id="billing_address1" type="hidden" value="<?= $address1 ?>">
<input name="billing_address2" id="billing_address2" type="hidden" value="<?= $address2 ?>">
<input name="billing_city" id="billing_city" type="hidden" value="<?= $city ?>">
<input name="billing_state" id="billing_state" type="hidden" value="<?= $state ?>">
<input name="zip" id="zip" type="hidden" value="<?= $zip ?>">

<div class="applied-to-my-invoice">
  <label>
    <input name="room" id="room" type="checkbox" value="">I would prefer not to share a room, and understand that a single supplement charge will be applied to my invoice.
  </label>
</div>
</div>
<div class="c"></div>
<div class="c"></div>
<div class="error-message">
<?php if(!empty($PayPalResult['L_SHORTMESSAGE0'])) { echo $PayPalResult['L_SHORTMESSAGE0']."<br/>"; }
    if(!empty($PayPalResult['L_LONGMESSAGE0'])) { echo $PayPalResult['L_LONGMESSAGE0']; }
?>
</div>
<div class="book-new-box">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/book-new-box-title-logo.png">Payment Information</h4>
    <div class="book-new-box-form">
    
    <div>
      <div class="book-new-box-form-inner-left">
        <div class="payment-detail-price">
          <span>Amount to be Charged</span>
            <?php echo $rate; ?> usd Deposit x <?php echo $guest; ?> Guest
            <span>= <?php echo $amt; ?> usd</span>
</div>
      <p>
          <label>Credit Card Type <span>*</span></label>
            <select name="card" id="card">
              <option>Select Card Type</option>
              <option value="MasterCard">Master Card</option>
              <option value="Visa">Visa</option>
              <option value="CreditCard">Credit Card</option>
            </select>
            <?php if(isset($card_err)) { ?><div class="error-message"> <?php echo $card_err ?></div> <?php } ?>
        </p>
        
      <p>
          <label>Card Number <span>*</span></label>
      <input name="cnumber" id="cnumber" type="text" required="required">
      <?php if(isset($cnumber)) { ?><div class="error-message"> <?php echo $cnumber ?></div> <?php } ?>
        </p>
        
      <div class="card-expire-date">
          <p>
            <label>Expiration Date <span>*</span></label>
            <select name="expmonth" id="expmonth">
              <option>Month</option>
              <option value="01">1</option>
              <option value="02">2</option>
              <option value="03">3</option>
              <option value="04">4</option>
              <option value="05">5</option>
              <option value="06">6</option>
              <option value="07">7</option>
              <option value="08">8</option>
              <option value="09">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
            </select>
            
            <select name="expyear" name="expyear">
              <option>Year</option>
              <?php $j=date('Y');
              $k = $j+20;
              for($j;$j<$k;$j++) { ?>
                <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
              <?php } ?>
            </select>
            <div class="c"></div>
            <?php if(isset($date)) { ?><div class="error-message"> <?php echo $date ?></div> <?php } ?>
            </p>
        </div>
      <p>
          <label>Card Verification # ? <span>*</span></label>
      <input name="verify" name="verify" type="text" required="required">
      <?php if(isset($verify)) { ?><div class="error-message"> <?php echo $verify ?></div> <?php } ?>
        </p>
        </div>
      <!-- <div class="book-new-box-form-inner-right">
        
      <p>
          <label>Name <span>*</span> <b>(as it appers on credit card)</b></label>
      <input name="" type="text">
          <div class="book-new-box-form-check-box">
            <label><input name="" type="checkbox">Billing address same as guest 1</label>
            </div>
        </p>
        
      <p>
          <label>Country</label>
            <select name="">
              <option>United States</option>
              <option>2</option>
              <option>3</option>
            </select>
        </p>
        
      <p>
          <label>Address <b>(line 2 )</b></label>
      <input name="" type="text">
        </p>
        
      <p>
          <label>City</label>
      <input name="" type="text">
        </p>
        
      <p>
          <label>State</label>
            <select name="">
              <option>Select</option>
              <option>2</option>
              <option>3</option>
            </select>
        </p>
        
      <p>
          <label>Zip</label>
      <input name="" type="text">
        </p>
        
      <p>
          <label>Day Time Phone <span>*</span> <b>(as printed in passport)</b></label>
      <input name="" type="text">
        </p>
        </div> -->
        <div class="c"></div>
    </div>
        
    </div>
</div>

<div class="c"></div>

<div class="book-new-box">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/book-new-box-title-logo.png">Terms And Conditions</h4>
<div class="book-new-box-tc">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Submission of your Request is merely  an offer to The Sample Network to use commercially reasonable efforts to provide the services and deliver the deliverables described in the Request for the price proposed therein. The Request is not binding on The Sample Network unless or until The Sample Network delivers written confirmation of its acceptance of the Request to the Client. The Sample Network reserves the right, in its sole discretion, to accept or reject any Request. Acceptance of a Request  only obligats The Sample Network to use commercially reasonable efforts to provide the services described in the Request. In the event The Sample Network accepts a Request, Client shall have thirty (60) days to make its survey available to The Sample Network as required by the Request.
</div>
<div class="book-new-box-tc-accepted">
<div class="book-new-box-form-check-box">
  <label><span><a href="javascript:;" class="print" data-id =<?= "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>>Print this page</a></span>
    <input name="terms" id="terms" type="hidden" value="0">
    <input name="terms" id="terms" type="checkbox" value="1">I have read and accepted the <a href="#">Terms and Conditions</a>
    <?php if(isset($terms)) { ?><div class="error-message"> <?php echo $terms ?></div> <?php } ?>
  </label>
</div>
</div>
</div>
  </div>
  
<div class="make-a-reservation-onlne-text">
  *Trip cancellation insurance
<p>We strongly recommend purchasing trip insurance that covers cancellation and interruptions. Many trip insurance policies also include medical expenses, emergency evacuation, and protection for baggage and personal possessions.</p>
<p>After submitting your booking form and when your payment has been verified, you will receive a receipt number on the next page. A copy of your booking will be sent to you by email to the email address you provided.</p>
</div>
  
<div class="c"></div>
<div align="right">
  <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/google-captcha.jpg"> -->
  <input type="submit" value="submit" name="submit" class="payment-submit">
</div>
</form>
  <div class="c"></div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('a.print').click(function() {
    var url = $(this).data('id');
    var pp = window.open(url,'_blank',"toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500,height=600,width=640");
    });
});
 </script>
<?php
get_footer();