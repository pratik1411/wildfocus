<?php
/**
 * Template Name: Photo Tours Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul>
        <li style="width:100% !important; height:551px; overflow:hidden; background:url(<?php echo get_template_directory_uri(); ?>/images/photo-tour-bg.jpg) no-repeat center top">
  <!-- <video preload="auto" autoplay loop style="width:2560px !important; height:1580px; margin-top:-300px; z-index:1;" >
        <source src="<?php echo get_template_directory_uri(); ?>/penguin-clip.mp4" type="video/mp4"/>
        <source src="<?php echo get_template_directory_uri(); ?>/penguin-clip.ogv" type="video/ogg"/>
  </video>  -->
    <div class="photo-tours-banner-text">
              <div class="photo-tours-banner-text-inner" style="z-index:1546;">
                <?php while(have_posts()):the_post(); ?>
                  <?php the_content(); ?>
                <?php endwhile; ?>
              </div>
            </div>
        </li>
        </ul>
    </div>
</div>
<section class="photo-tours">
  <?php   $taxonomy = 'tour-category';
      $term_args=array(
        'hide_empty' => false,
        'orderby' => 'name',
        'order' => 'ASC'
      );
      $tax_terms = get_terms($taxonomy,$term_args); 
  ?>
  <?php foreach ($tax_terms as $tax_term) { ?>
  <div class="photo-tours-list">
      <h2><?php echo $tax_term->name; ?><img src="<?php echo z_taxonomy_image_url($tax_term->term_id); ?>"></h2>
      <ul>
        <?php query_posts("post_type=tour&taxonomy=tour-category&posts_per_page=-1&term=$tax_term->slug"); ?>
        <?php while(have_posts()):the_post(); ?>
          <li><a href="<?php echo get_permalink(); ?>">
                <h3><?php the_title(); ?></h3>
                <div class="photo-tours-list-img"><?php the_post_thumbnail(); ?></div>
                <div class="photo-tours-list-disc">
                  <?php 
                    $stdt=get_field("startdate"); 
                    $enddt=get_field("enddate");
                    $datetime1 = new DateTime($stdt);
                    $datetime2 = new DateTime($enddt);
                    $interval = date_diff($datetime1, $datetime2);
                   ?>
                  <span><?php echo $interval->format('%a days'); ?></span>
                    <div><?php echo date("F d-", strtotime("$stdt")); ?>
                    <?php $stm=date("F", strtotime("$stdt"));
                          $endm=date("F", strtotime("$enddt"));
                    if($stm==$endm): ?>
                    <?php $end=date("d, Y", strtotime("$enddt")); echo $end; ?>
                    <?php else: 
                      echo date("F d, Y", strtotime("$enddt")); 
                      endif;?>
                    </div>
                    <div></div>
                </div>
              </a>
           </li>
        <?php endwhile; wp_reset_query(); ?>
      </ul>
      <div class="c"></div>
    </div>
    <?php } ?>
</section>
<?php
get_footer();