<?php
/**
 * Template Name: Booking Confirmed Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
          <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=booking-confirmed&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
          <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
  <h2><?php the_title(); ?></h2>
  <div class="make-a-reservation-onlne">
      <div class="make-a-reservation-onlne-text">
          <b>Thank you for booking your photography holiday with Wild Focus!</b>
      </div>
<div class="c"></div>
<div class="c"></div>
<div class="book-new-box" id="test">
  <h4><img src="<?php echo get_template_directory_uri(); ?>/images/booking-confirmed-logo.jpg"><span>Iterinary Receipt</span></h4>
  <div class="booking-confirmed-content">
      <h5>Booking Details</h5>
        <div class="booking-details-content">
          <div class="booking-details-content-left">
              <ul>
                  <li><span>Booking Date</span>: Friday 10 Jun 2014</li>
                  <li><span>Address</span>: WildFocus Tours</li>
                  <li><span>Contact</span>: +61 2 8003 7453</li>
                  <li><span>Email</span>: <a href="mailto:info@wildfocus.com">info@wildfocus.com</a></li>
                </ul>
            </div>
          <div class="booking-details-content-right">
              Booking Reference Number:
                <span>4560399</span>
            <p>Status:
                  <span>Confirmed</span>
                </p>
            </div>
            <div class="c"></div>
        </div>
    </div>
    
<div class="booking-confirmed-content">
        <div class="guest-detail-content">
      <h5>Guest Details</h5>
          <ol>
              <li>John Doe (Adult)</li>
              <li>John Doe (Adult)</li>
            </ol>
        </div>
    </div>
    
<div class="booking-confirmed-content">
        <div class="guest-detail-content">
      <h5>Tour Details</h5>
          <div class="booking-confirmed-tour-detail">
                  <div class="booking-confirmed-tour-detail-name">Tour Name: Bhutan Photography Expedition</div>
              <div class="booking-confirmed-tour-detail-left">
                    <span>Departure :</span>
                    Monday 12 Jan 2014, 8.00am
                </div>
                
              <div class="booking-confirmed-tour-detail-right">
                <span>Arrival :</span>
                Monday 12 Jan 2014, 8.00am
                </div>
                <div class="c"></div>
            </div>
            
        </div>
    </div>
    
<div class="booking-confirmed-content booking-confirmed-last">
        <div class="guest-detail-content">
      <h5>Payment Details</h5>
          <div class="booking-confirmed-payment-details">
              <div class="booking-confirmed-payment-details-left">
                  <div class="booking-confirmed-payment-cost">
                      Cost : 1,200.00 USD
                        <span>Tax (10%): 120.0 USD</span>
                    </div>
                    <div class="booking-confirmed-payment-cost-total">TOTAL : 1,320.00 USD</div>
                    <div class="c"></div>
                </div>
                
              <div class="booking-confirmed-payment-details-right">
                  <ul>
                      <li><span>Payment Type</span>: Credit Card</li>
                      <li><span>Date</span>: 10 Jun, 2014</li>
                      <li><span>Transaction ID</span>: 00992881920</li>
                    </ul>
                </div>
                <div class="c"></div>
            </div>
        </div>
    </div>
    
</div>

<div class="print-btn">
<input type="button" value="" onclick="PrintElem('#test')" />
</div>
<div class="c"></div>
  </div>
</section>
<?php
get_footer();