<?php
/**
 * Template Name: Our travel styles Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
        <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
        data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=our-travel-style&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
	<h2>Why Travel With Wild?</h2>
    <div class="trip-details-left">
    	<div class="trip-details-left-nav">
        	<ul>
            	<li><a href="<?php echo get_site_url(); ?>/reasons-to-travel/">10 Reasons to Travel With Us</a></li>
            	<li><a href="<?php echo get_site_url(); ?>/our-travel-styles/" class="active">Our Travel Styles</a></li>
            	<li><a href="<?php echo get_site_url(); ?>/sustainable-travel/">Sustainable Travel</a></li>
            	<li><a href="<?php echo get_site_url(); ?>/giving-back-to-the-wild/">Giving Back to the Wild</a></li>
            	<li><a href="<?php echo get_site_url(); ?>/why-travelwith-wild/">With us</a></li>
            	<li><a href="<?php echo get_site_url(); ?>/amazing-destinations/" >Amazing Destinations</a></li>
            </ul>
        </div>
    </div>
    <div class="trip-details-right">
    <div class="amazing-destinations-text">
    <img src="<?php the_field('titleimg'); ?>">
    <div class="c"></div>
    <?php while(have_posts()):the_post(); ?>
        <?php echo get_the_content(); ?>
    <?php endwhile; ?>
		<div class="c"></div>        
      </div>        
        <div class="organitation-we-support">
        	<?php query_posts("post_type=style&posts_per_page=-1&order=Asc"); ?>
        	<ul>
        	<?php while(have_posts()):the_post(); ?>
           	  <li>
                	<div class="organitation-we-support-left"><?php the_post_thumbnail("full"); ?></div>
                	<div class="organitation-we-support-right">
                    	<h4><?php the_title(); ?></h4>
                        <?php the_Content(); ?>
                        <p>Feature Photo Adventure: <strong><?php echo get_excerpt("250"); ?></strong></p>
                </div>
                    <div class="c"></div>
              </li>
             <?php endwhile; wp_reset_query(); ?>
        	</ul>
        </div>
  <?php the_post_thumbnail("ourtravel"); ?> </div>
    <div class="c"></div>
</section>
<?php
get_footer();
