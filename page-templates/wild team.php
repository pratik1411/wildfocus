<?php
/**
 * Template Name: Wild Team Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="fade"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
        >
          <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=team&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
            <li>
              <?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>
<section class="wild-team" id="tabs">
  <div class="wild-team-left">
      <div class="wild-team-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/the-wild-team.png"></div>
      <?php query_posts("post_type=team&posts_per_page=-1&order=Asc"); ?>
      <?php $i=1; ?>
       <ul class="wild-team-tabs">
            <?php while(have_posts()):the_post(); ?>            
              <li><a href="#tabs-<?php echo $i; ?>"><span></span><img src="<?php the_field('iconimg'); ?>"><?php the_title(); ?></a></li>
            <?php $i++; endwhile; wp_reset_query(); ?>
            <div class="c"></div>
        </ul>
  </div>
  <div class="wild-team-right">
    <div class="wild-team-disc">
      <?php query_posts("post_type=team&posts_per_page=-1&order=Asc"); ?>
      <?php $j=1; ?>
      <?php while(have_posts()):the_post(); ?> 
      <div id="tabs-<?php echo $j; ?>">
        <div class="team-disc-icon-top"><img src="<?php echo get_template_directory_uri(); ?>/images/girrae-icon.png"></div>
          <div>
            <div class="team-member-photo"><?php the_post_thumbnail(array(248,263)); ?></div>
            <div class="team-member-proile">
                <h3><?php the_title(); ?></h3>
                <?php the_field('short_desc'); ?>
              </div>
              <div class="c"></div>
          </div>
          <div class="team-profile-disc">
            <?php echo get_the_content(); ?>
          </div>
      </div>
    <?php $j++; endwhile; wp_reset_query(); ?>
  </div>    
    </div>
    <div class="c"></div>
    <div class="wild-team-motto">
      <div class="wild-team-motto-title"></div>
      <div>
          <div class="wild-team-motto-left"><img src="<?php the_field('motto_image'); ?>"></div>
          <div class="wild-team-motto-right">
              <div class="wild-team-motto-right-title">Explore <span>&#9679;</span> Discover <span>&#9679;</span> Capture</div>
                <div class=""><?php the_field('motto_desc'); ?></div>
            </div>
            <div class="c"></div>
        </div>
    </div>
    <div>
        <div class="what-is-so-wild-about-us-title"></div>
      <div class="what-is-so-wild-about-us">
          <div class="what-is-so-wild-about-us-sub-title"><img src="<?php echo get_template_directory_uri(); ?>/images/wild-university-report-card.png"></div>
            <div class="what-is-so-wild-about-us-text">
              <?php the_field("report_text"); ?>
              <span><?php the_field("report_author"); ?></span>
            </div>
        <div class="wild-university-report-card-ranting">
          <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
            
        <div class="wild-university-report-card-grade">
          <ul>
            <li><span>Awesome<br>Adventures:</span><?php the_field("adventure"); ?></li>
            <li><span>Fun<br>Factor:</span><?php the_field("fun"); ?></li>
            <li><span>Photography<br>Stuff:</span><?php the_field("photography"); ?></li>
          </ul>
        </div>
            
      </div>
      
      <div class="we-love-about-wild-focus-title"><img src="<?php echo get_template_directory_uri(); ?>/images/we-love-about-wild-focus-title.png"></div>
      <div class="we-love-about-wild-focus-title-sm"><img src="<?php echo get_template_directory_uri(); ?>/images/we-love-about-wild-focus-title-sm.png"></div>
      
      <div>
          <div class="wild-team-motto-left"><?php the_post_thumbnail(array(244,266)); ?></div>
          <div class="wild-team-motto-right">
            <div class="">
              <?php while(have_posts()):the_post(); ?>
                <?php echo get_the_content(); ?>
              <?php endwhile; ?>
            </div>
        </div>
            <div class="c"></div>
      </div>
      
      <div class="wild-photo-montage-title"><img src="<?php echo get_template_directory_uri(); ?>/images/wild-photo-montage.png"></div>
      <div class="wild-photo-montage-title-sm"><img src="<?php echo get_template_directory_uri(); ?>/images/wild-photo-montage-sm.png"></div>
      <div class="wild-photo-video">
        <div class="wild-photo-video-frame"></div>
          <?php the_field("video"); ?>
      </div>
  </div>
</section>
<?php
get_footer();