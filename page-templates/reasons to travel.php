<?php
/**
 * Template Name: Reasons to travel Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner-wrap">
  <div class="banner banner-inner">
      <ul class="banner-slider"
        data-cycle-slides=">li"
        data-cycle-fx="scrollHorz"
        data-cycle-carousel-fluid=true
        data-cycle-prev=".banner-prev"
        data-cycle-next=".banner-next"
    data-cycle-pager=".banner-pager"
        >
           <?php query_posts("post_type=slide&taxonomy=slideshow&posts_per_page=-1&term=reasons-to-travel&order=asc"); ?>
           <?php while ( have_posts() ) : the_post(); ?>
        		<li>
              	<?php the_post_thumbnail("full",array("class"=>"banner_img1")); ?>
              <img src="<?php the_field('mobile_image'); ?>" class="banner_img2"/>
            </li>
        <?php endwhile; wp_reset_query(); ?>
        </ul>
    </div>
</div>

<section class="trip-details">
  <h2>Why Travel With Wild?</h2>
    <div class="trip-details-left">
      <div class="trip-details-left-nav">
          <ul>
              <li><a href="<?php echo get_site_url(); ?>/reasons-to-travel/" class="active">10 Reasons to Travel With Us</a></li>
              <li><a href="<?php echo get_site_url(); ?>/our-travel-styles/">Our Travel Styles</a></li>
              <li><a href="<?php echo get_site_url(); ?>/sustainable-travel/">Sustainable Travel</a></li>
              <li><a href="<?php echo get_site_url(); ?>/giving-back-to-the-wild/">Giving Back to the Wild</a></li>
              <li><a href="<?php echo get_site_url(); ?>/why-travelwith-wild/">With us</a></li>
              <li><a href="<?php echo get_site_url(); ?>/amazing-destinations/" >Amazing Destinations</a></li>
            </ul>
        </div>
    </div>
    
    <div class="trip-details-right">
      <div class="reasons-to-travel">
          <div class="reasons-to-travel-title"><img src="<?php echo get_template_directory_uri(); ?>/images/10-reasons-to-travel-title.jpg"></div>
          <?php query_posts("post_type=reason&posts_per_page=-1&order=Asc"); ?>
          <ul>
          <?php while(have_posts()):the_post(); ?>
              <li>
                 <?php the_content(); ?>
            </li>
          <?php endwhile; wp_reset_query(); ?>
          </ul>
        </div>
    </div>
    
    <div class="c"></div>
</section>
<?php
get_footer();
