<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php $region=$_GET["region"]; 
	$interest=$_GET["interest"]; 
	$st= $_GET["st"]; 
	$en= $_GET["end"];
	$str_stdt=strtotime($st);
	$str_enddt=strtotime($en);
?>
<section class="photo-tours">
	<div class="photo-tours-list">
			<h2><?php printf( __( "Tour Search Results for: $region")); ?></h2>
			<?php   $taxonomy = 'tour-category';
				$term_args=array(
				'hide_empty' => false,
				'orderby' => 'name',
				'order' => 'ASC'
				);
				$tax_terms = get_terms($taxonomy,$term_args); 
			?>
			<?php foreach ($tax_terms as $tax_term) { ?>
				<?php if($tax_term->name==$_GET["region"]): ?>
					<h2><?php echo $tax_term->name; ?><img src="<?php echo z_taxonomy_image_url($tax_term->term_id); ?>"></h2>
				<?php else: continue; endif; ?>
			<?php } ?>	
			<?php $count=query_posts("post_type=tour&taxonomy=tour-category&posts_per_page=-1&term=".$region); ?>
			<?php //if ( $count->found_posts ) : ?>
			<ul>
			<?php while ( have_posts() ) : the_post();?>
				<?php $post_data = get_post($post->ID, ARRAY_A);
					$slug = $post_data['post_name'];
				?>
				<?php 
	                $stdt=get_field("startdate"); 
	                $enddt=get_field("enddate");
	                $str_stdt_tour=strtotime($stdt);
					$str_enddt_tour=strtotime($enddt);
	                $datetime1 = new DateTime($stdt);
	                $datetime2 = new DateTime($enddt);
	                $interval = date_diff($datetime1, $datetime2);
	                //if ( $str_stdt>=$str_stdt_tour && $str_enddt <= $str_enddt_tour && $slug==$interest ):
	                if ( $slug==$interest ):
                ?>
				<li><a href="<?php echo get_permalink(); ?>">
		                <h3><?php the_title(); ?></h3>
		                <div class="photo-tours-list-img"><?php the_post_thumbnail(); ?></div>
		                <div class="photo-tours-list-disc">
		                  <span><?php echo $interval->format('%a days'); ?></span>
		                    <div><?php echo date("F d-", strtotime("$stdt")); ?>
		                    <?php $stm=date("F", strtotime("$stdt"));
		                          $endm=date("F", strtotime("$enddt"));
		                    if($stm==$endm): ?>
		                    	<?php $end=date("d, Y", strtotime("$enddt")); echo $end; ?>
		                    <?php else: 
		                      	echo date("F d, Y", strtotime("$enddt")); 
		                    endif;?>
		                    </div>
		                    <div></div>
		                </div>
	              	</a>
           		</li>
			<?php endif; endwhile; wp_reset_query(); ?>
			</ul>
		<?php //else : ?>
			<!-- <p>No results found.</p> -->
		<?php //endif; ?>
			<div class="c"></div>
    	</div>
</section>
<?php
get_footer();
